package com.example.tsvqt.photosharing.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.LoginActivity;
import com.example.tsvqt.photosharing.Activities.MainActivity;
import com.example.tsvqt.photosharing.Activities.SettingsActivity;
import com.example.tsvqt.photosharing.Activities.UserAboutActivity;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserProfileTab extends Fragment implements UploadTab.DataChangedListener, AddDeleteFromAlbumDialogFragment.ImageDeleteListener{

    public static UserProfileTab instance = null;

    public UserProfileTab() {
        // Required empty public constructor
    }

    public static UserProfileTab newInstance() {
        if(instance==null){
            instance = new UserProfileTab();
        }
        return instance;
    }

    public static UserProfileTab getInstance(){
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private static final String TAG = UserProfileTab.class.getSimpleName();

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private Toolbar toolBarUserProfile;
    private ImageView ivProfileCoverImage, ivUserAvatar;
    private TextView tvUserName, tvImageCount;
    private String fName, lName, avatarURL, userPageImageURL, email, userID, imageCount, created_at;
    private TabLayout tabLayout;
    private FragmentActivity myContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(SessionManager.isLoggedIn()) {
            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_user_profile_tab, container, false);
            setHasOptionsMenu(true);
            getUserDataFromServer();

            ivProfileCoverImage = view.findViewById(R.id.ivProfileCoverImage);
            ivUserAvatar = view.findViewById(R.id.ivUserAvatar);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvImageCount = view.findViewById(R.id.tvUploadedImageCount);

            mViewPager = view.findViewById(R.id.vpUserProfilePager);
            mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
            //UserImagesTab userImagesTab = UserImagesTab.getInstance();
            //userImagesTab.addListener(this);
            mSectionsPagerAdapter.addFragment(new UserImagesTab(), "Images");
            mSectionsPagerAdapter.addFragment(new UserAlbumsTab(), "Albums");
            mSectionsPagerAdapter.addFragment(new UserFavedTab(), "Faved");
            mViewPager.setAdapter(mSectionsPagerAdapter);

            tabLayout = view.findViewById(R.id.userProfileTabLayout);
            tabLayout.setupWithViewPager(mViewPager);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            //IF LOGGED IN USER show this below ELSE Show You Don't have a profile. Or better remove the TAB from the viewPager.

            toolBarUserProfile = view.findViewById(R.id.tool_bar_user_profile);
            toolBarUserProfile.inflateMenu(R.menu.menu_logged_user_profile);
            toolBarUserProfile.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        //Change the ImageView image source depends on menu item click
                        case R.id.item_logout:
                            logoutUser();
                            return true;
                        case R.id.item_settings:
                            Intent intentSettings = new Intent(getContext(), SettingsActivity.class);
                            HashMap<String, String> loggedUser = SharedPreferencesHandler.getUserDetails();
                            intentSettings.putExtra("avatarImage", loggedUser.get(SharedPreferencesHandler.KEY_AVATAR_IMAGE));
                            intentSettings.putExtra("coverImage",  loggedUser.get(SharedPreferencesHandler.KEY_USER_PAGE_IMAGE));
                            intentSettings.putExtra("firstName",  loggedUser.get(SharedPreferencesHandler.KEY_FNAME));
                            intentSettings.putExtra("lastName",  loggedUser.get(SharedPreferencesHandler.KEY_LNAME));
                            intentSettings.putExtra("email,", loggedUser.get(SharedPreferencesHandler.KEY_EMAIL));
                            startActivity(intentSettings);
                            return true;
                        case R.id.item_about:
                            Intent intentAbout = new Intent(getContext(), UserAboutActivity.class);
                            intentAbout.putExtra("imgCount", imageCount);
                            intentAbout.putExtra(SharedPreferencesHandler.KEY_UID, userID);
                            intentAbout.putExtra(SharedPreferencesHandler.KEY_CREATED_AT, created_at);
                            startActivity(intentAbout);
                            return true;
                    }
                    return false;
                }
            });

            CollapsingToolbarLayout collapsingToolbarLayout = view.findViewById(R.id.col_bar_user_profile);
            collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

            return view;
        }else{
            return inflater.inflate(R.layout.fragment_no_user, container, false);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onRefresh() {
        UserImagesTab userImagesTab = (UserImagesTab) mSectionsPagerAdapter.getItem(0);
        userImagesTab.getImageListFromServer(SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID));
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private final SparseArray<WeakReference<Fragment>> instantiatedFragments = new SparseArray<>();
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title){
            if(!mFragmentList.contains(fragment)) {
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            }
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @NonNull
        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            final Fragment fragment = (Fragment) super.instantiateItem(container, position);
            instantiatedFragments.put(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Override
        public void destroyItem(final ViewGroup container, final int position, final Object object) {
            instantiatedFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i("FragCreateList","onCreateOptionsMenu called");
        inflater.inflate(R.menu.menu_logged_user_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void getUserDataFromServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_userProfileData";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_USERPROFILE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "UserProfileTab Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        // data successfully retrieved from server
                        JSONObject user = jObj.getJSONObject("user");
                        userID = user.getString("id");
                        imageCount = user.getString("imageCount");
                        created_at = user.getString("created_at");

                        HashMap<String, String> loggedUser = SharedPreferencesHandler.getUserDetails();
                        if(userID.equals(loggedUser.get(SharedPreferencesHandler.KEY_UID))){
                            Glide.with(getContext()).load(AppConfig.URL_AVATAR_STORAGE + loggedUser.get(SharedPreferencesHandler.KEY_AVATAR_IMAGE)).apply(RequestOptions.circleCropTransform()).into(ivUserAvatar);
                            String coverImage = loggedUser.get(SharedPreferencesHandler.KEY_USER_PAGE_IMAGE);
                            Glide.with(getContext()).load(AppConfig.URL_USER_PAGE_IMAGE + coverImage).apply(RequestOptions.centerCropTransform()).into(ivProfileCoverImage);
                            String userName = loggedUser.get(SharedPreferencesHandler.KEY_FNAME) + " " + (loggedUser.get(SharedPreferencesHandler.KEY_LNAME));
                            tvUserName.setText(userName);
                            tvImageCount.setText("Images " + imageCount);
                        }else{
                            Toast.makeText(getContext(),
                                    "Users do not match!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        //Error. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getContext(),
                               errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "UserProfileTab Error: " + error.getMessage());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                //ID IS LOGGED USER
                HashMap<String, String> user = SharedPreferencesHandler.getUserDetails();
                params.put("userID", user.get(SharedPreferencesHandler.KEY_UID));
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void logoutUser() {
        SessionManager.setLogin(false);
        SharedPreferencesHandler.clearSharedPreferences();

        // Launching the login activity
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


}
