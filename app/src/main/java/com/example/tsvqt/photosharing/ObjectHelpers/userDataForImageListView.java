package com.example.tsvqt.photosharing.ObjectHelpers;

public class userDataForImageListView {

    private String id, fullName, userProfilePictureUrl;

    public userDataForImageListView(String id, String fName, String lName, String avatar_image){
        this.id = id;
        this.fullName = fName + " " + lName;
        this.userProfilePictureUrl = "http://192.168.1.102/storage/avatar_images/" + avatar_image;
    }

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getUserProfilePictureUrl() {
        return userProfilePictureUrl;
    }
}
