package com.example.tsvqt.photosharing.ObjectHelpers;

import com.example.tsvqt.photosharing.helper.AppConfig;

public class ProfileData {

    private String fName,lName,id,avatarURL,coverURL,imageCount;

    public ProfileData(String fName, String lName, String id, String avatarURL, String coverURL, String imageCount) {
        this.fName = fName;
        this.lName = lName;
        this.id = id;
        this.avatarURL = AppConfig.URL_AVATAR_STORAGE + avatarURL;
        this.coverURL = AppConfig.URL_USER_PAGE_IMAGE + coverURL;
        this.imageCount = imageCount;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getUserName(){
        return fName + " " + lName;
    }

    public String getId() {
        return id;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public String getImageCount() {
        return imageCount;
    }
}
