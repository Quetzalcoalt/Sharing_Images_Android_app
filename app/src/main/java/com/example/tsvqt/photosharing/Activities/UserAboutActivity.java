package com.example.tsvqt.photosharing.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UserAboutActivity extends AppCompatActivity {

    private static final String TAG = UserAboutActivity.class.getSimpleName();

    private TextView tvImageCount, tvDateJoined;
    private ImageView ivBack, ivPopular1, ivPopular2, ivPopular3;
    private String userID, imgID1, imgID2, imgID3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_about);

        Intent getIntent = getIntent();
        userID = getIntent.getStringExtra(SharedPreferencesHandler.KEY_UID);

        tvImageCount = findViewById(R.id.ivImageCount);
        tvDateJoined = findViewById(R.id.ivDateJoined);
        ivPopular1 = findViewById(R.id.ivPopular1);
        ivPopular2 = findViewById(R.id.ivPopular2);
        ivPopular3 = findViewById(R.id.ivPopular3);
        ivBack = findViewById(R.id.ivBack);

        tvImageCount.setText(getIntent.getStringExtra("imgCount"));
        tvDateJoined.setText(getIntent.getStringExtra(SharedPreferencesHandler.KEY_CREATED_AT));

        getUserAboutInfo();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivPopular1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserAboutActivity.this, OpenPictureActivity.class);
                intent.putExtra("imageID", imgID1);
            }
        });

        ivPopular2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserAboutActivity.this, OpenPictureActivity.class);
                intent.putExtra("imageID", imgID2);
            }
        });

        ivPopular3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserAboutActivity.this, OpenPictureActivity.class);
                intent.putExtra("imageID", imgID3);
            }
        });
    }

    private void getUserAboutInfo(){
        // Tag used to cancel the request
        String tag_string_req = "req_userAboutInfo";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GET_MOST_POPULAR_IMAGES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "UserAboutActivity Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray jArr = jObj.getJSONArray("images");
                        boolean error = jObj.getBoolean("error");
                        // Check for error node in json
                        if (!error) {
                            // data successfully retrieved from server
                            int arrayLength = jArr.length();
                            switch (arrayLength) {
                                case 1:
                                    Glide.with(UserAboutActivity.this).load(AppConfig.URL_IMAGE_STORAGE + jArr.getJSONObject(0).getString("image")).apply(new RequestOptions().centerCrop()).into(ivPopular1);
                                    imgID1 = jArr.getJSONObject(0).getString("id");
                                    break;
                                case 2:
                                    Glide.with(UserAboutActivity.this).load(AppConfig.URL_IMAGE_STORAGE + jArr.getJSONObject(0).getString("image")).apply(new RequestOptions().centerCrop()).into(ivPopular1);
                                    Glide.with(UserAboutActivity.this).load(AppConfig.URL_IMAGE_STORAGE + jArr.getJSONObject(1).getString("image")).apply(new RequestOptions().centerCrop()).into(ivPopular2);
                                    imgID1 = jArr.getJSONObject(0).getString("id");
                                    imgID2 = jArr.getJSONObject(1).getString("id");
                                    break;
                                case 3:
                                    Glide.with(UserAboutActivity.this).load(AppConfig.URL_IMAGE_STORAGE + jArr.getJSONObject(0).getString("image")).apply(new RequestOptions().centerCrop()).into(ivPopular1);
                                    Glide.with(UserAboutActivity.this).load(AppConfig.URL_IMAGE_STORAGE + jArr.getJSONObject(1).getString("image")).apply(new RequestOptions().centerCrop()).into(ivPopular2);
                                    Glide.with(UserAboutActivity.this).load(AppConfig.URL_IMAGE_STORAGE + jArr.getJSONObject(2).getString("image")).apply(new RequestOptions().centerCrop()).into(ivPopular3);
                                    imgID1 = jArr.getJSONObject(0).getString("id");
                                    imgID2 = jArr.getJSONObject(1).getString("id");
                                    imgID3 = jArr.getJSONObject(2).getString("id");
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            // Error. Get the error message
                            String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                            Toast.makeText(getApplicationContext(),
                                    errorMsg, Toast.LENGTH_LONG).show();
                        }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                params.put("userID", userID);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
