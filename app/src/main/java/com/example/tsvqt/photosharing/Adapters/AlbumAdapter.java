package com.example.tsvqt.photosharing.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.AddImageToAlbumsActivity;
import com.example.tsvqt.photosharing.Activities.EditAlbumDetailsActivity;
import com.example.tsvqt.photosharing.Activities.OpenAlbumActivity;
import com.example.tsvqt.photosharing.Activities.OpenPictureActivity;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.ObjectHelpers.ProfileImages;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_FIRST_USER;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {

    private static final String TAG = AlbumAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<AlbumData> albumList = Collections.emptyList();
    private Context context;
    private PopupMenu menu;

    public AlbumAdapter(Context context, List<AlbumData> albumList){
        inflater = LayoutInflater.from(context);
        this.albumList = albumList;
        this.context = context;
        Log.e(TAG, "IS THIS EVERY CALLED");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.gallary_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        Log.e(TAG, "IS THIS EVERY CALLED");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder called " + position);

        Log.e(TAG, "IS THIS " + position);

        AlbumData album = albumList.get(position);

        holder.tvAlbumTitle.setText(album.getAlbumImageTitle());
        holder.tvAlbumCreatedAt.setText(album.getCreated_at());
        holder.tvAlbumImageCount.setText("Images " + album.getImageCount());
        Glide.with(context).load(album.getAlbumImageURL()).apply(new RequestOptions().centerCrop()).into(holder.ivGallary);

        holder.ivGallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlbumData alb = albumList.get(position);
                Intent intent = new Intent(context, OpenAlbumActivity.class);
                intent.putExtra("albumID", alb.getId());
                intent.putExtra("albumTitle", alb.getAlbumImageTitle());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.ivGallary.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                menu = new PopupMenu(context, holder.ivGallary);
                menu.getMenuInflater().inflate(R.menu.menu_long_ress_album, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        final AlbumData album = albumList.get(position);
                        switch (item.getItemId()) {
                            case R.id.item_open:
                                Intent intent = new Intent(context, OpenAlbumActivity.class);
                                intent.putExtra("albumID", album.getId());
                                intent.putExtra("albumTitle", album.getAlbumImageTitle());
                                ((Activity)context).startActivityForResult(intent, RESULT_FIRST_USER);
                                context.startActivity(intent);
                                return true;
                            case R.id.item_edit:
                                Intent intent1 = new Intent(context, EditAlbumDetailsActivity.class);
                                intent1.putExtra("albumID",album.getId());
                                intent1.putExtra("title", album.getAlbumImageTitle());
                                intent1.putExtra("description", album.getDescription());
                                intent1.putExtra("tags", album.getTags());
                                intent1.putExtra("access", album.getAccess());
                                intent1.putExtra("imageName", album.getAlbumImageURL());
                                //startActivityForResult(intent1, 1);
                                ((Activity)context).startActivityForResult(intent1, RESULT_FIRST_USER);
                                return true;
                            case R.id.item_delete:
                                AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
                                alertbox.setMessage("Are you sure you want to delete this image ?");
                                alertbox.setTitle("Warning");
                                alertbox.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                deleteAlbumFromServer(album.getId());
                                            }
                                        });
                                alertbox.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                //Do nothing
                                            }
                                        });
                                alertbox.show();
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                AlbumData album = albumList.get(position);
                if (album.getUserID().equals(SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID))) {
                    menu.show();
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivGallary;
        private TextView tvAlbumTitle, tvAlbumCreatedAt, tvAlbumImageCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ivGallary = itemView.findViewById(R.id.iv_addGallary);
            tvAlbumTitle = itemView.findViewById(R.id.tv_gallaryTitle);
            tvAlbumCreatedAt = itemView.findViewById(R.id.tv_gallaryCreatedDate);
            tvAlbumImageCount = itemView.findViewById(R.id.tv_gallaryImageCount);
        }
    }

    private void deleteAlbumFromServer(final String albumID){
        // Tag used to cancel the request
        String tag_string_req = "req_delete_image";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEALBUM, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "EditAlbumDetailsActivity Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if(!error) {
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();
                    }else{
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "EditImageDetailsActivity Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                params.put("albumID", albumID);
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
