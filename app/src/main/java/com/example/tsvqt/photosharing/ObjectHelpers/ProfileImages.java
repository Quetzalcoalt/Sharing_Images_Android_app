package com.example.tsvqt.photosharing.ObjectHelpers;

import com.example.tsvqt.photosharing.helper.AppConfig;

public class ProfileImages {

    private String imageTitle, ImageURL, id, user_id;

    public ProfileImages(String id, String imageTitle, String url, String user_id){
        this.imageTitle = imageTitle;
        this.ImageURL = AppConfig.URL_IMAGE_STORAGE + url;
        this.id = id;
        this.user_id = user_id;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }
}
