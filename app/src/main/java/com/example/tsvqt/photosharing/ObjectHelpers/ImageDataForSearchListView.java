package com.example.tsvqt.photosharing.ObjectHelpers;

import com.example.tsvqt.photosharing.helper.AppConfig;

public class ImageDataForSearchListView {

    private final String TAG = "ImageDataForImageListView";
    private String imgID, imgUrl;

    public ImageDataForSearchListView(String imgID, String imgName){
        this.imgID = imgID;
        this.imgUrl = AppConfig.URL_IMAGE_STORAGE + imgName;
    }

    public String getImgID() {
        return imgID;
    }

    public String getImgUrl() {
        return imgUrl;
    }

}
