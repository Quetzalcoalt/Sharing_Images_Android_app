package com.example.tsvqt.photosharing.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();
    private AlertDialog.Builder alertDialogBuider;
    private ProgressDialog pDialog;
    private final int AVATARGALLERY = 1;
    private final int AVATARCAMERA = 2;
    private final int COVERGALLERY = 3;
    private final int COVERCAMERA = 4;
    private Bitmap avatarBitmap;
    private Bitmap coverBitmap;
    private String imageFilePath = "";

    private ImageView ivAvatar,ivCover, ivBack;
    private EditText etFirstName,etLastName,etOldPassword,etNewPassword,etConfirmPassword;
    private Button bSave;
    private boolean isAvatarGallery = false, isAvatarCamera = false, isCoverGallery = false, isCoverCamera = false;

    //TODO if this is successfull update the local sharedpreferences and update UserPfoile with new data and feedtab.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        ivAvatar = findViewById(R.id.ivChangedAvatar);
        ivCover = findViewById(R.id.ivChangedCover);
        etFirstName = findViewById(R.id.etChangedFirstName);
        etLastName = findViewById(R.id.etChangedLastName);
        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        bSave = findViewById(R.id.bSaveAccountChanges);
        ivBack = findViewById(R.id.ivBack);

        Intent intent = getIntent();

        Glide.with(this).load(AppConfig.URL_AVATAR_STORAGE + intent.getStringExtra("avatarImage")).apply(RequestOptions.circleCropTransform()).into(ivAvatar);
        String coverImage = intent.getStringExtra("coverImage");
        if(coverImage.contains(".jpg")) {
            Glide.with(this).load(AppConfig.URL_USER_PAGE_IMAGE + coverImage).apply(RequestOptions.centerCropTransform()).into(ivCover);
        }else{
            ivCover.setImageDrawable(null);
            if(coverImage.equals("")){
                ivCover.setBackgroundColor(Color.parseColor("#636b6f"));
            }else
                ivCover.setBackgroundColor(Color.parseColor(coverImage));
        }
        etFirstName.setHint(intent.getStringExtra("firstName"));
        etLastName.setHint(intent.getStringExtra("lastName"));

        ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuider = new AlertDialog.Builder(SettingsActivity.this);
                alertDialogBuider.setTitle("Choose from")
                        .setItems(R.array.gallery_camera, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        choosePhotoFromGallery(AVATARGALLERY);
                                        break;
                                    case 1:
                                        takePhotoFromCamera(AVATARCAMERA);
                                        break;
                                    default: Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                alertDialogBuider.show();
            }
        });

        ivCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuider = new AlertDialog.Builder(SettingsActivity.this);
                alertDialogBuider.setTitle("Choose from")
                        .setItems(R.array.gallery_camera, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        choosePhotoFromGallery(COVERGALLERY);
                                        break;
                                    case 1:
                                        takePhotoFromCamera(COVERCAMERA);
                                        break;
                                    default: Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                alertDialogBuider.show();
            }
        });

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog.setMessage("Updating settings!");
                showDialog();
                updateUser();
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateUser(){
            // Tag used to cancel the request
            String tag_string_req = "req_UpdateUser";

            StringRequest strReq = new StringRequest(Request.Method.POST,
                    AppConfig.URL_UPDATEUSERDETAILS, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "SettingsActivity Response: " + response);
                    hideDialog();
                    try {
                        JSONObject jObj = new JSONObject(response);
                        boolean error = jObj.getBoolean("error");
                        // Check for error node in json
                        if(!error) {
                            Toast.makeText(getApplicationContext(), jObj.getString("error_msg") + "!", Toast.LENGTH_LONG).show();
                            ivAvatar.setImageDrawable(getResources().getDrawable(R.drawable.ic_upload));
                            ivCover.setImageDrawable(getResources().getDrawable(R.drawable.ic_upload));
                            SharedPreferencesHandler.updateUser(jObj.getString("fName"),jObj.getString("lName"),jObj.getString("avatarImage"), jObj.getString("coverImage"));
                            HashMap<String, String> user = SharedPreferencesHandler.getUserDetails();
                            finish();
                        }else{
                            String errorMsg = jObj.getString("error_msg");
                            Toast.makeText(getApplicationContext(),
                                    errorMsg, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                        //Toast.makeText(getActivity().getApplicationContext(),
                        //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Log.e(TAG, "SettingsActivity Error: " + error.getMessage());
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        error.getMessage(), Toast.LENGTH_LONG).show();
                }
            })

            {
                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to android update user profile php url
                    Map<String, String> params = new HashMap<String, String>();
                    HashMap<String, String> user = SharedPreferencesHandler.getUserDetails();

                    String fName = etFirstName.getText().toString();
                    String lName = etLastName.getText().toString();
                    if(fName.equals("")){
                        params.put("fName", "");
                    }else {
                        params.put("fName", fName);
                    }
                    if(lName.equals("")){
                        params.put("lName", "");
                    }else {
                        params.put("lName", lName);
                    }
                    if(isAvatarCamera || isAvatarGallery) {
                        params.put("avatarImage", bitmapToString(avatarBitmap));
                        avatarBitmap.recycle();
                    }else{
                        params.put("avatarImage", "");
                    }
                    params.put("avatarImageName", "avatar" + user.get("email"));
                    if(isCoverCamera || isCoverGallery) {
                        params.put("coverImage", bitmapToString(coverBitmap));
                        coverBitmap.recycle();
                    }else{
                        params.put("coverImage", "");
                    }
                    params.put("coverImageName", "cover" + user.get("email"));
                    params.put("oldPassword", etOldPassword.getText().toString());
                    params.put("newPassword", etNewPassword.getText().toString());
                    params.put("confirmedPassword", etConfirmPassword.getText().toString());
                    params.put("userID", user.get("uid"));

                    return params;
                }
            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

    private void choosePhotoFromGallery(int g){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, g);
    }

    private void takePhotoFromCamera(int c){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try{
            photoFile = createImageFile();
        }catch (IOException e){
            e.printStackTrace();
        }
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));//Send fileUri with intent
        startActivityForResult(takePictureIntent, c);//start activity for result with CAMERA_REQUEST_CODE
    }

    private File createImageFile() throws IOException{
        String timeStamp = DateFormat.getDateInstance().toString();
        String imageFileName ="IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        imageFilePath = image.getAbsolutePath();
        Log.d(TAG, "----------------------------------- filePath = " + imageFilePath);
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED){
            return;
        }
        if (requestCode == AVATARGALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    isAvatarGallery = true;
                    isAvatarCamera = false;
                    avatarBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI);
                    Glide.with(this).load(avatarBitmap).apply(RequestOptions.circleCropTransform()).into(ivAvatar);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == AVATARCAMERA) {
            isAvatarCamera = true;
            isAvatarGallery = false;
            avatarBitmap = BitmapFactory.decodeFile(imageFilePath);
            Glide.with(this).load(avatarBitmap).apply(RequestOptions.circleCropTransform()).into(ivAvatar);

            //TODO make so the imageView has height of wrap_content
        }
        if (requestCode == COVERGALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    isCoverGallery = true;
                    isCoverCamera = false;
                    coverBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI);
                    Glide.with(this).load(coverBitmap).into(ivCover);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == COVERCAMERA) {
            isCoverGallery = false;
            isCoverCamera = true;
            coverBitmap = BitmapFactory.decodeFile(imageFilePath);
            Glide.with(this).load(coverBitmap).into(ivCover);

            //TODO make so the imageView has height of wrap_content
        }
    }

    private String bitmapToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes,Base64.DEFAULT);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}

