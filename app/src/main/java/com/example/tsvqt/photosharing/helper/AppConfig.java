package com.example.tsvqt.photosharing.helper;

public class AppConfig {

        // Server user login url
        public static String URL_LOGIN = "http://85.187.115.27/android/login.php";

        // Server user register url
        public static String URL_REGISTER = "http://85.187.115.27/android/register.php";

        // Server home page url
        public static String URL_HOMEPAGE = "http://85.187.115.27/android/Homepage.php";

        //When clicked on an image, open the image url, this gets the image user and comments data
        public static String URL_OPEN_PICTURE = "http://85.187.115.27/android/openpicture.php";

        //The storage where all the images are located
        public static String URL_IMAGE_STORAGE = "http://85.187.115.27/storage/cover_images/";

        //The storage where all the user profile pictures are located
        public static String URL_AVATAR_STORAGE = "http://85.187.115.27/storage/avatar_images/";

        //The storage where all the user profile pictures are located
        public static String URL_USER_PAGE_IMAGE = "http://85.187.115.27/storage/user_page_images/";

        //Inserts the comment to a photo id
        public static String URL_INSERT_COMMENT = "http://85.187.115.27/android/insertComment.php";

        //Get the comments from a photo id
        public static String URL_GET_COMMENT = "http://85.187.115.27/android/getComments.php";

        //Get Data for userProfile tab, This means All images, albums, about info, ect ect
        public static String URL_USERPROFILE = "http://85.187.115.27/android/getProfileData.php";

        //Gets Data from android to insert image in database.
        public static String URL_UPLOADIMAGE = "http://85.187.115.27/android/uploadImage.php";

        //Update image data
        public static String URL_UPDATEIMAGE = "http://85.187.115.27/android/updateImage.php";

        //Update user data
        public static String URL_UPDATEUSERDETAILS = "http://85.187.115.27/android/updateUserDetails.php";

        //Detele Image
        public static String URL_DELETEIMAGE = "http://85.187.115.27/android/deleteImage.php";

        //Get 3 most popular photos
        public static String URL_GET_MOST_POPULAR_IMAGES = "http://85.187.115.27/android/getUserPopularImages.php";

        //Update favouries
        public static String URL_UPDATE_FAVOURIES = "http://85.187.115.27/android/updateFav.php";

        //Get user favourite images
        public static String URL_FAVED_IMAGES = "http://85.187.115.27/android/getUserFavedImages.php";

        //Get user images
        public static String URL_USERIMAGES = "http://85.187.115.27/android/getUserImages.php";

        //Get Albums
        public static String URL_ALBUMS = "http://85.187.115.27/android/getAlbums.php";

        //Upload new Album
        public static String URL_UPLOADALBUM = "http://85.187.115.27/android/createAlbum.php";

        //Storage folder for album images
        public static String URL_ALBUM_IMAGE_STORAGE = "http://85.187.115.27/storage/album_images/";

        //Add Image to albums
        public static String URL_ADDTOALBUM = "http://85.187.115.27/android/addImageToAlbums.php";

        //get Album images
        public static String URL_GET_ALBUM_IMAGES = "http://85.187.115.27/android/getAlbumImages.php";

        //edit Album
        public static String URL_EDITALBUM = "http://85.187.115.27/android/editAlbum.php";

        //delete Album
        public static String URL_DELETEALBUM = "http://85.187.115.27/android/deleteAlbum.php";

        //getImagesFromSearch
        public static String URL_SEARCHIMAGES = "http://85.187.115.27/android/searchImages.php";

        //getUsersFromSearch
        public static String URL_SEARCHUSERS = "http://85.187.115.27/android/searchUsers.php";

        //getAlbumsFromSearch
        public static String URL_SEARCHIALBUMS = "http://85.187.115.27/android/searchAlbums.php";

}
