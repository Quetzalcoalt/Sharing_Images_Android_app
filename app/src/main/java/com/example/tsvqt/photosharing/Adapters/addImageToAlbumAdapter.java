package com.example.tsvqt.photosharing.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.OpenAlbumActivity;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class addImageToAlbumAdapter extends RecyclerView.Adapter<addImageToAlbumAdapter.ViewHolder> {

    private static final String TAG = addImageToAlbumAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<AlbumData> albumList = Collections.emptyList();
    private ArrayList<String> addToAlbumsList;
    private Context context;

    public addImageToAlbumAdapter(Context context, List<AlbumData> albumList){
        inflater = LayoutInflater.from(context);
        this.albumList = albumList;
        this.context = context;
        addToAlbumsList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.add_to_albums_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder called " + position);

        final AlbumData album = albumList.get(position);

        holder.tvAlbumTitle.setText(album.getAlbumImageTitle());
        holder.tvAlbumImageCount.setText("Images " + album.getImageCount());
        Glide.with(context).load(album.getAlbumImageURL()).apply(new RequestOptions().centerCrop()).into(holder.ivGallary);
        if(album.isIn()) {
            holder.cbAddToAlbum.setChecked(true);
            addToAlbumsList.add(album.getId());
        }else{
            holder.cbAddToAlbum.setChecked(false);
        }

        holder.cbAddToAlbum.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    album.setChecked(true);
                    addToAlbumsList.add(album.getId());
                }else{
                    album.setChecked(false);
                    addToAlbumsList.remove(album.getId());
                }
            }
        });

        holder.ivGallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlbumData alb = albumList.get(position);
                Intent intent = new Intent(context, OpenAlbumActivity.class);
                intent.putExtra("albumID", alb.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public List<String> getAddToAlbumsList(){
        return addToAlbumsList;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivGallary;
        private TextView tvAlbumTitle, tvAlbumImageCount;
        private CheckBox cbAddToAlbum;

        public ViewHolder(View itemView) {
            super(itemView);
            ivGallary = itemView.findViewById(R.id.iv_addGallary);
            tvAlbumTitle = itemView.findViewById(R.id.tv_gallaryTitle);
            cbAddToAlbum = itemView.findViewById(R.id.cbAddToAlbum);
            tvAlbumImageCount = itemView.findViewById(R.id.tv_gallaryImageCount);
        }
    }

}
