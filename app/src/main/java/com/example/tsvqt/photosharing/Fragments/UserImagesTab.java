package com.example.tsvqt.photosharing.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.load.engine.Resource;
import com.example.tsvqt.photosharing.Adapters.UserImagesAdapter;
import com.example.tsvqt.photosharing.ObjectHelpers.ProfileImages;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import static com.android.volley.VolleyLog.TAG;

public class UserImagesTab extends Fragment{

    private static final String TAG = UserImagesTab.class.getSimpleName();

    private static UserImagesTab instance = null;
    private AddDeleteFromAlbumDialogFragment.ImageDeleteListener listener;

    public UserImagesTab() {
        // Required empty public constructor
    }

    public static UserImagesTab getInstance() {
        if(instance==null){
            instance = new UserImagesTab();
        }
        Log.e(TAG, "OMG TVA ^TE GO ..... " + instance.getId());
        return instance;
    }

    public void addListener(AddDeleteFromAlbumDialogFragment.ImageDeleteListener listener){
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProfileImagesList = new ArrayList<>();
    }

    private UserImagesAdapter adapter;
    private ArrayList<ProfileImages> ProfileImagesList;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TextView tvNoImagesFound;
    private String user;

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_images_tab, container, false);
        view.setId(5);

        tvNoImagesFound = view.findViewById(R.id.tvNoImages);
        tvNoImagesFound.setVisibility(View.GONE);

        mRecyclerView = view.findViewById(R.id.rvUserImages);
        adapter = new UserImagesAdapter(getActivity(), ProfileImagesList, listener);
        layoutManager = new GridLayoutManager(getActivity(),3);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(adapter);

        //mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        user = SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID);
        if(getActivity() != null){
            if(getActivity().getIntent() != null) {
                Intent intent = getActivity().getIntent();
                if (intent.getStringExtra("userId") != null) {
                    user = intent.getStringExtra("userId");
                }
            }
        }
        getImageListFromServer(user);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener = null;
    }

    /*
        @Override
        public void updateList() {
            Log.e(TAG, "UPDATE LIST CALLED!");
            getImageListFromServer(SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID));
        }
    */
    public void getImageListFromServer(final String user){
        // Tag used to cancel the request
        String tag_string_req = "req_userProfileImages";
        ProfileImagesList.clear();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_USERIMAGES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "UserImagesTab Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    //get Albums also
                    //get user about page.
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {

                        //check these in the helper DB tam onova
                        JSONArray jArr = jObj.getJSONArray("images");
                        for(int i = 0; i < jArr.length();i++) {
                            String id = jArr.getJSONObject(i).getString("id");
                            String title = jArr.getJSONObject(i).getString("title");
                            String cover_image = jArr.getJSONObject(i).getString("cover_image");
                            String user_id = jArr.getJSONObject(i).getString("user_id");
                            /**TODO
                             *THIS ONLY WHEN CLICKED ON A PFORILE< NOT YOUR PROFILE
                             *IF NOT LOGGED IN SHOW ONLY public photos
                             *IF LOGGED IN CHECK if user is friends with current  profile viewed. Do some magic here or in the PHP code (maybe faster there)
                             *String access = jArr.getJSONObject(i).getString("access");
                             * **/

                            ProfileImages image = new ProfileImages(id, title, cover_image, user_id);
                            ProfileImagesList.add(image);
                        }
                    } else {
                        // Error. Get the error message
                        //String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                        //Toast.makeText(getActivity().getApplicationContext(),
                        //       errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                if(ProfileImagesList.size() == 0){
                    tvNoImagesFound.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }else {
                    adapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "UserProfileTab Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                //ID IS LOGGED USER
                params.put("userID", user);
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

/*
    @Override
    public void update(Observable o, Object arg) {
        Log.e(TAG, "OBSERVER CALLED!");
        getImageListFromServer(SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID));
    }
*/

}
