package com.example.tsvqt.photosharing.ObjectHelpers;

import com.example.tsvqt.photosharing.helper.AppConfig;

public class ImageDataForImageListView {

    private final String TAG = "ImageDataForImageListView";
    private String imgUrl, imgTitle, uploadDate, userID, access, imgID, favCounter, commentsCounter;
    private boolean isFaved;
    private userDataForImageListView userData;

    public ImageDataForImageListView(String imgID, String title, String userID, String imgName, String uploadDate, String access, String favCounter, String commentsCounter,boolean isFaved, userDataForImageListView userData){
        this.imgID = imgID;
        this.imgTitle = title ;
        this.userID = userID;
        this.imgUrl = AppConfig.URL_IMAGE_STORAGE + imgName;
        this.uploadDate = uploadDate;
        this.access = access;
        this.userData = userData;
        this.favCounter = favCounter;
        this.commentsCounter = commentsCounter;
        this.isFaved = isFaved;
    }

    public String getAccess() {
        return access;
    }

    public String getImgID() {
        return imgID;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getImageTitle() {
        return imgTitle;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public userDataForImageListView getUserData(){
        return userData;
    }

    public String getUserID() {
        return userID;
    }

    public String getFavCounter() {
        return favCounter;
    }

    public String getCommentsCounter() {
        return commentsCounter;
    }

    public boolean isFaved() {
        return isFaved;
    }

    public void setFaved(boolean faved) {
        isFaved = faved;
    }
}
