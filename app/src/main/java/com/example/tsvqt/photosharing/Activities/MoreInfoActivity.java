package com.example.tsvqt.photosharing.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tsvqt.photosharing.R;

public class MoreInfoActivity extends AppCompatActivity {

    private TextView tvTitle, tvDescription, tvUplodedDate, tvUserName, tvTags;
    private ImageView ivBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info);

        tvTitle = findViewById(R.id.tvMFimageTitle);
        tvDescription = findViewById(R.id.tvMFDescription);
        tvUplodedDate = findViewById(R.id.tvMFcreated_at);
        tvUserName = findViewById(R.id.tvMFUserName);
        tvTags = findViewById(R.id.tvMFtags);
        ivBackButton = findViewById(R.id.ivBack);

        Intent getIntent = getIntent();
        tvTitle.setText(getIntent.getStringExtra("title"));
        tvDescription.setText(getIntent.getStringExtra("description"));
        tvUplodedDate.setText(getIntent.getStringExtra("uploadDate"));
        tvUserName.setText(getIntent.getStringExtra("userName"));
        tvTags.setText(getIntent.getStringExtra("tags"));

        //TODO make a Recycled View or something to load all Albums taken from OpenPictureActivity or from here with a separate request.

        tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO open userName activity when ready
                Toast.makeText(MoreInfoActivity.this, tvUserName.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
