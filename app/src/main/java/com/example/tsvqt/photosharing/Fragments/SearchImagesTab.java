package com.example.tsvqt.photosharing.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Adapters.FeedAdapter;
import com.example.tsvqt.photosharing.Adapters.SearchImagesAdapter;
import com.example.tsvqt.photosharing.Adapters.SearchUserAdapter;
import com.example.tsvqt.photosharing.ObjectHelpers.ImageDataForImageListView;
import com.example.tsvqt.photosharing.ObjectHelpers.ImageDataForSearchListView;
import com.example.tsvqt.photosharing.ObjectHelpers.userDataForImageListView;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SearchImagesTab extends Fragment {
    private static final String TAG = FeedTab.class.getSimpleName();
    private static SearchImagesTab instance;

    public SearchImagesTab() {
        // Required empty public constructor
    }

    public static SearchImagesTab newInstance(){
        if(instance == null){
            instance = new SearchImagesTab();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private static ArrayList<ImageDataForSearchListView> imageUserHomeListView;
    private static SearchImagesAdapter adapter;
    private RecyclerView mRecyclerView;

    //TODO optimization, send first images ID from the list, in php check if the first image ID is not = to the sent image id, to not waste time filling new data.

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_images_tab, container, false);
        imageUserHomeListView = new ArrayList<>();

        mRecyclerView = view.findViewById(R.id.rvSearchImagesTab);
        adapter = new SearchImagesAdapter(getActivity(), imageUserHomeListView);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setItemViewCacheSize(16);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    public static void clearList(){
        if(adapter!=null) {
            imageUserHomeListView.clear();
            adapter.notifyDataSetChanged();
        }
    }

    public void getImageListFromServer(final String search){
        // Tag used to cancel the request

        String tag_string_req = "req_homeScreenImages";
        imageUserHomeListView.clear();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SEARCHIMAGES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, " Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray jArr = jObj.getJSONArray("images");

                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        // data successfully retrieved from server
                        for(int i = 0; i < jArr.length();i++) {
                            // Now store the user in SQLite
                            JSONObject currentObject = jArr.getJSONObject(i);
                            String id = currentObject.getString("id");
                            String cover_image = currentObject.getString("cover_image");

                            ImageDataForSearchListView image = new ImageDataForSearchListView(id,cover_image);
                            imageUserHomeListView.add(image);
                        }
                    } else {
                        // Error. Get the error message
                        String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                        Toast.makeText(getActivity().getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, " Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                params.put("search", search);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}