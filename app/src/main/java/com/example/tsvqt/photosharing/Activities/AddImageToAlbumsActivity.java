package com.example.tsvqt.photosharing.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Adapters.AlbumAdapter;
import com.example.tsvqt.photosharing.Adapters.addImageToAlbumAdapter;
import com.example.tsvqt.photosharing.Fragments.UserAlbumsTab;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddImageToAlbumsActivity extends AppCompatActivity {

    private static final String TAG = AddImageToAlbumsActivity.class.getSimpleName();

    private addImageToAlbumAdapter adapter;
    private ArrayList<AlbumData> albumList;
    private RecyclerView mRecyclerView;
    private ImageView ivBack, ivCreateAlbum;
    private Button bDone;
    private ProgressDialog pDialog;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image_to_albums);
        albumList = new ArrayList<>();

        ivBack = findViewById(R.id.ivBack);
        ivCreateAlbum = findViewById(R.id.iv_addGallary);

        mRecyclerView = findViewById(R.id.rvAlbums);
        adapter = new addImageToAlbumAdapter(this, albumList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        bDone = findViewById(R.id.bDone);
        bDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog.setMessage("Adding image to albums");
                showDialog();
                addImageToAlbums();
                //List<String> aa = adapter.getAddToAlbumsList();
                //aa.size();
            }
        });

        //adapter get list with album ids where image_id is in

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivCreateAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddImageToAlbumsActivity.this, CreateAlbumActivity.class);
                startActivity(intent);
            }
        });

        getAlbumListFromServer();
    }

    private void getAlbumListFromServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_Albums";
        albumList.clear();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ALBUMS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "AddImageToAlbumsActivity Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    userID = jObj.getString("user_id");
                    // Check for error node in json
                    if (!error) {
                        JSONArray jArr = jObj.getJSONArray("albums");
                        for(int i = 0; i < jArr.length();i++) {
                            String id = jArr.getJSONObject(i).getString("id");
                            String title = jArr.getJSONObject(i).getString("title");
                            String album_image = jArr.getJSONObject(i).getString("album_image");
                            String created_at = jArr.getJSONObject(i).getString("created_at");
                            String imageCount = jArr.getJSONObject(i).getString("image_count");
                            boolean isIn = jArr.getJSONObject(i).getBoolean("isIn");
                            String description = jArr.getJSONObject(i).getString("description");
                            String tags = jArr.getJSONObject(i).getString("tags");
                            String access = jArr.getJSONObject(i).getString("access");

                            AlbumData image = new AlbumData(id, title, album_image, created_at, imageCount, isIn, userID, description,tags,access);
                            albumList.add(image);
                        }
                    } else {
                        // Error. Get the error message
                        //String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                        //Toast.makeText(getActivity().getApplicationContext(),
                        //       errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "AddImageToAlbumsActivity Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                //ID IS LOGGED USER
                if(getIntent() != null) {
                    Intent intent = getIntent();
                    params.put("userID", SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID));
                    params.put("imageID", intent.getStringExtra("imageID"));
                }
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void addImageToAlbums(){
        // Tag used to cancel the request
        String tag_string_req = "req_Albums";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDTOALBUM, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "AddImageToAlbumsActivity Response: " + response);
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        //turn of activity ?
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    } else {
                        // Error. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "AddImageToAlbumsActivity Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                Intent intent = getIntent();
                params.put("imageID", intent.getStringExtra("imageID"));
                params.put("albumsList", new Gson().toJson(adapter.getAddToAlbumsList()));
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
