package com.example.tsvqt.photosharing.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.OpenUserActivity;
import com.example.tsvqt.photosharing.Activities.RegisterActivity;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.ObjectHelpers.CommentsData;

import java.util.List;

public class CommentsAdapter extends ArrayAdapter<CommentsData> {

    private static final String TAG = CommentsAdapter.class.getSimpleName();

    private int resource;
    private List<CommentsData> commentsList;

    //ViewHolder hold the R.id elements so we call them only once.
    static class ViewHolder {
        public TextView tvUserName;
        public TextView tvComment;
        public TextView tvWrittenOn;
        public ImageView userAvatar;
    }

    public CommentsAdapter(@NonNull Context context, int resource, @NonNull List<CommentsData> commentsList) {
        super(context, resource, commentsList);

        this.resource = resource;
        this.commentsList = commentsList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        CommentsData comment = commentsList.get(position);
        ViewHolder viewHolder;

        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(resource, parent, false);
            viewHolder.tvUserName = convertView.findViewById(R.id.tvUserName);
            viewHolder.tvComment = convertView.findViewById(R.id.tvComment);
            viewHolder.tvWrittenOn = convertView.findViewById(R.id.tvWrittenOn);
            viewHolder.userAvatar = convertView.findViewById(R.id.ivUserAvatar);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvUserName.setText(comment.getUsername());
        viewHolder.tvWrittenOn.setText(comment.getUploadDate());
        viewHolder.tvComment.setText(comment.getComment());
        Glide.with(getContext()).load(comment.getAvatarURL()).apply(RequestOptions.circleCropTransform()).into(viewHolder.userAvatar);

        viewHolder.userAvatar.setOnClickListener(userAvatarClickListener);
        viewHolder.tvUserName.setOnClickListener(userNameClickListener);

        return convertView;
    }

    private View.OnClickListener userAvatarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parentRow = (View) v.getParent();
            ConstraintLayout layout = (ConstraintLayout) parentRow.getParent();
            ListView listVIew = (ListView) layout.getParent();
            final int position = listVIew.getPositionForView(parentRow);
            CommentsData comment = commentsList.get(position);
            Intent intent = new Intent(getContext(), OpenUserActivity.class);
            intent.putExtra("userId", comment.getUserID());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getContext().startActivity(intent);
        }
    };

    private View.OnClickListener userNameClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parentRow = (View) v.getParent();
            LinearLayout layout1 = (LinearLayout) parentRow.getParent();
            ConstraintLayout layout2 = (ConstraintLayout) layout1.getParent();
            ListView listVIew = (ListView) layout2.getParent();
            final int position = listVIew.getPositionForView(parentRow);
            CommentsData comment = commentsList.get(position);
            Intent intent = new Intent(getContext(), OpenUserActivity.class);
            intent.putExtra("userId", comment.getUserID());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getContext().startActivity(intent);
        }
    };
}
