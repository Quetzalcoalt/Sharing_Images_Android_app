package com.example.tsvqt.photosharing.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.OpenAlbumActivity;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.ObjectHelpers.SearchAlbumData;
import com.example.tsvqt.photosharing.R;

import java.util.Collections;
import java.util.List;

public class SearchAlbumAdapter extends RecyclerView.Adapter<SearchAlbumAdapter.ViewHolder>{

    private static final String TAG = SearchAlbumAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<SearchAlbumData> albumList = Collections.emptyList();
    private Context context;

    public SearchAlbumAdapter(Context context, List<SearchAlbumData> albumList){
        inflater = LayoutInflater.from(context);
        this.albumList = albumList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.gallary_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder called " + position);

        SearchAlbumData album = albumList.get(position);

        holder.tvAlbumTitle.setText(album.getAlbumImageTitle());
        holder.tvAlbumCreatedAt.setText(album.getCreated_at());
        holder.tvAlbumImageCount.setText("Images " + album.getImageCount());
        Glide.with(context).load(album.getAlbumImageURL()).apply(new RequestOptions().centerCrop()).into(holder.ivGallary);

        holder.ivGallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchAlbumData alb = albumList.get(position);
                Intent intent = new Intent(context, OpenAlbumActivity.class);
                intent.putExtra("albumID", alb.getId());
                intent.putExtra("albumTitle", alb.getAlbumImageTitle());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivGallary;
        private TextView tvAlbumTitle, tvAlbumCreatedAt, tvAlbumImageCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ivGallary = itemView.findViewById(R.id.iv_addGallary);
            tvAlbumTitle = itemView.findViewById(R.id.tv_gallaryTitle);
            tvAlbumCreatedAt = itemView.findViewById(R.id.tv_gallaryCreatedDate);
            tvAlbumImageCount = itemView.findViewById(R.id.tv_gallaryImageCount);
        }
    }
}
