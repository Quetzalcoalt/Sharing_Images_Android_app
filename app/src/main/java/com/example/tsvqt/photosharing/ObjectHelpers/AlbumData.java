package com.example.tsvqt.photosharing.ObjectHelpers;

import com.example.tsvqt.photosharing.helper.AppConfig;

public class AlbumData {

    private String albumImageTitle, AlbumImageURL, id ,created_at, imageCount, userID,description,tags,access;
    private boolean isIn;

    public AlbumData(String id, String albumImageTitle, String AlbumImageURL, String created_at, String imageCount, boolean isIn, String userID, String description, String tags, String access){
        this.albumImageTitle = albumImageTitle;
        this.AlbumImageURL = AppConfig.URL_ALBUM_IMAGE_STORAGE + AlbumImageURL;
        this.id = id;
        this.created_at = created_at;
        this.imageCount = imageCount;
        this.isIn = isIn;
        this.userID = userID;
        this.description = description;
        this.tags = tags;
        this.access = access;
    }

    public String getAlbumImageTitle() {
        return albumImageTitle;
    }

    public String getAlbumImageURL() {
        return AlbumImageURL;
    }

    public String getId() {
        return id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getImageCount() {
        return imageCount;
    }

    public boolean isIn() {
        return isIn;
    }

    public void setChecked(boolean isChecked){
        if(isChecked){
            isIn = true;
        }else {
            isIn = false;
        }
    }

    public String getUserID() {
        return userID;
    }

    public String getDescription() {
        return description;
    }

    public String getTags() {
        return tags;
    }

    public String getAccess() {
        return access;
    }
}
