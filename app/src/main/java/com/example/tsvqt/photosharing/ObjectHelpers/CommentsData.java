package com.example.tsvqt.photosharing.ObjectHelpers;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.SessionManager;

public class CommentsData{

    private String username,uploadDate, avatarURL,comment,userID;

    public CommentsData(String username, String uploadDate, String avatarURL, String comment, String userID){
        this.username = username;
        this.uploadDate = uploadDate;
        this.avatarURL = AppConfig.URL_AVATAR_STORAGE + avatarURL;
        this.comment = comment;
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public String getComment() {
        return comment;
    }

    public String getUserID() {
        return userID;
    }

}
