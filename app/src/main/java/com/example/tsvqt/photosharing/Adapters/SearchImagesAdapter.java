package com.example.tsvqt.photosharing.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.CommentsActivity;
import com.example.tsvqt.photosharing.Activities.OpenPictureActivity;
import com.example.tsvqt.photosharing.Activities.OpenUserActivity;
import com.example.tsvqt.photosharing.ObjectHelpers.ImageDataForSearchListView;
import com.example.tsvqt.photosharing.ObjectHelpers.userDataForImageListView;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.ObjectHelpers.ImageDataForImageListView;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchImagesAdapter extends RecyclerView.Adapter<SearchImagesAdapter.ViewHolder>{

    private static final String TAG = SearchImagesAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<ImageDataForSearchListView> imageList = Collections.emptyList();
    private Context context;

    public SearchImagesAdapter(Context context, List<ImageDataForSearchListView> imageList){
        inflater = LayoutInflater.from(context);
        this.imageList = imageList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.search_images_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder called " + position);

        ImageDataForSearchListView image = imageList.get(position);

        Glide.with(context).load(image.getImgUrl()).into(holder.ivSearchImage);

        holder.ivSearchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageDataForSearchListView img = imageList.get(position);
                Intent intent = new Intent(context, OpenPictureActivity.class);
                intent.putExtra("imageID", img.getImgID());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivSearchImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ivSearchImage = itemView.findViewById(R.id.ivSearchImage);
        }
    }

}
