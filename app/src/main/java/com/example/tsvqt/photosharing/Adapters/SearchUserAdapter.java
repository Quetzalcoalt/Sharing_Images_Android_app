package com.example.tsvqt.photosharing.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.OpenUserActivity;
import com.example.tsvqt.photosharing.ObjectHelpers.UserSearchData;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;

import java.util.Collections;
import java.util.List;

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.ViewHolder> {

    private static final String TAG = SearchUserAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<UserSearchData> imageList = Collections.emptyList();
    private Context context;

    public SearchUserAdapter(Context context, List<UserSearchData> imageList) {
        inflater = LayoutInflater.from(context);
        this.imageList = imageList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.search_users_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder called " + position);

        UserSearchData user = imageList.get(position);

        holder.tvUserName.setText(user.getName());
        holder.tvImageCount.setText(user.getCountUploadedPhotos() + " Photos - ");
        holder.rvViewsCount.setText(user.getTotalImageViews() + " Views");
        Glide.with(context).load(AppConfig.URL_AVATAR_STORAGE + user.getImage()).apply(RequestOptions.circleCropTransform()).into(holder.ivUserAvatar);

        holder.tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserSearchData user = imageList.get(position);
                Intent intent = new Intent(context, OpenUserActivity.class);
                intent.putExtra("userId", user.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.ivUserAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserSearchData user = imageList.get(position);
                Intent intent = new Intent(context, OpenUserActivity.class);
                intent.putExtra("userId", user.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return imageList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvUserName, tvImageCount, rvViewsCount;
        private ImageView ivUserAvatar;

        public ViewHolder(View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvImageCount = itemView.findViewById(R.id.tvUploadedImageCount);
            rvViewsCount = itemView.findViewById(R.id.tvViewsCount);
            ivUserAvatar = itemView.findViewById(R.id.ivUserAvatar);
        }
    }
}
