package com.example.tsvqt.photosharing.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

public class CreateAlbumActivity extends AppCompatActivity {

    private static final String TAG = EditImageDetailsActivity.class.getSimpleName();
    private AlertDialog.Builder alertDialogBuider;
    private final int GALLERY = 1;
    private final int CAMERA = 2;
    private Bitmap uploadImageBitmap;
    private String imageFilePath;
    private ProgressDialog pDialog;

    private ImageView ivBack, ivAlbum;
    private EditText etAlbumTitle, etTags, etDescription;
    private Button uploadButton;
    private final String PUBLIC = "Public";
    private final String PRIVATE = "Private";
    private final String FRIENDSONLY = "Friends Only";
    private Spinner spinnerUpload;
    private int access = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_album);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        ivBack = findViewById(R.id.ivBack);
        ivAlbum = findViewById(R.id.iv_albumImage);
        etAlbumTitle = findViewById(R.id.etAlbumTitle);
        etDescription = findViewById(R.id.etDescription);
        etTags = findViewById(R.id.etTags);
        uploadButton = findViewById(R.id.bEditAlbum);
        spinnerUpload = findViewById(R.id.sUpload);

        String[] accessArray = new String[]{
                PUBLIC,
                FRIENDSONLY,
                PRIVATE
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CreateAlbumActivity.this, R.layout.spinner_item, accessArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUpload.setAdapter(adapter);

        spinnerUpload.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                switch (item) {
                    case PUBLIC:
                        access = 0;
                        break;
                    case FRIENDSONLY:
                        access = 2;
                        break;
                    case PRIVATE:
                        access = 1;
                        break;
                    default:
                        access = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ivAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuider = new AlertDialog.Builder(CreateAlbumActivity.this);
                alertDialogBuider.setTitle("Choose from")
                        .setItems(R.array.gallery_camera, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        choosePhotoFromGallery();
                                        break;
                                    case 1:
                                        takePhotoFromCamera();
                                        break;
                                    default:
                                        Toast.makeText(CreateAlbumActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                alertDialogBuider.show();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO check all data before upload
                pDialog.setMessage("Creating Album!");
                showDialog();
                uploadAlbumToServer();
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void choosePhotoFromGallery(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try{
            photoFile = createImageFile();
        }catch (IOException e){
            e.printStackTrace();
        }

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));//Send fileUri with intent
        startActivityForResult(takePictureIntent, CAMERA);//start activity for result with CAMERA_REQUEST_CODE
    }

    private File createImageFile() throws IOException{
        String timeStamp = DateFormat.getDateInstance().toString();
        String imageFileName ="IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        imageFilePath = image.getAbsolutePath();

        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED){
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    uploadImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    Glide.with(this).load(uploadImageBitmap).into(ivAlbum);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            uploadImageBitmap = BitmapFactory.decodeFile(imageFilePath);
            Glide.with(this).load(uploadImageBitmap).into(ivAlbum);

            //TODO make so the imageView has height of wrap_content
        }
    }

    private String bitmapToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes,Base64.DEFAULT);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void uploadAlbumToServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_UploadImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPLOADALBUM, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "CreateAlbumActivity Response: " + response);
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if(!error) {
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(CreateAlbumActivity.this,
                                errorMsg, Toast.LENGTH_LONG).show();
                        ivAlbum.setImageDrawable(getResources().getDrawable(R.drawable.ic_upload));
                        etAlbumTitle.setText("");
                        etTags.setText("");
                        etDescription.setText("");
                        finish();
                        //TODO show the user a text that the image was uploaded successfully and reset the uploadTab.
                    }else{
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(CreateAlbumActivity.this,
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(CreateAlbumActivity.this,
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "CreateAlbumActivity Error: " + error.getMessage());
                //Toast.makeText(CreateAlbumActivity.this,
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                HashMap<String, String> user = SharedPreferencesHandler.getUserDetails();

                params.put("title", etAlbumTitle.getText().toString());
                params.put("description", etDescription.getText().toString());
                params.put("tags", etTags.getText().toString());
                params.put("access", "" + access);
                params.put("albumImage", bitmapToString(uploadImageBitmap));
                params.put("userID", user.get("uid"));
                uploadImageBitmap.recycle();

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
