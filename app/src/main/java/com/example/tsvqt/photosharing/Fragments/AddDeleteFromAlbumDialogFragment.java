package com.example.tsvqt.photosharing.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Activities.AddImageToAlbumsActivity;
import com.example.tsvqt.photosharing.Activities.OpenPictureActivity;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddDeleteFromAlbumDialogFragment extends DialogFragment implements AdapterView.OnItemClickListener {

    private static final String TAG = AddDeleteFromAlbumDialogFragment.class.getSimpleName();
    private ListView listView;
    private String[] listItems = {"Open", "Add or Remove from album", "Delete"};

    public AddDeleteFromAlbumDialogFragment(){

    }

    public static AddDeleteFromAlbumDialogFragment newInstance(String imageID){
        AddDeleteFromAlbumDialogFragment fragment = new AddDeleteFromAlbumDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", "Choose from: ");
        args.putString("imageID", imageID);
        fragment.setArguments(args);
        return fragment;
    }


    @SuppressLint("ResourceType")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popup_menu_user_image, container);
        listView = view.findViewById(R.id.lvFragDialog);
        String title = getArguments().getString("title", "Choose from:");
        getDialog().setTitle(title);
        //Log.e(TAG, "Parent target is: " + getChildFragmentManager().findFragmentById(5).toString());
        //setTargetFragment(getChildFragmentManager().findFragmentById(5),1);
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,listItems);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void deleteImageFromServer(final String imageID){
        // Tag used to cancel the request
        String tag_string_req = "req_delete_image";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEIMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "UserImagesAdapter Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if(!error) {
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                        Log.e(TAG, errorMsg);
                        //UserImagesTab fragment = (UserImagesTab) getChildFragmentManager().findFragmentByTag("Images");
                        //fragment.getImageListFromServer(SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID));
                        //onImageDeletedListener.updateList();
                        onImageDeleteListener.onRefresh();
                        //TODO show the user a text that the image was uploaded successfully and reset the uploadTab.
                    }else{
                        String errorMsg = jObj.getString("error_msg");
                        //Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "UserImagesAdapter Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                params.put("imageID", imageID);
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        dismiss();
        switch (position){
            case 0:
                Intent intent = new Intent(getActivity(), OpenPictureActivity.class);
                intent.putExtra("imageID", getArguments().getString("imageID"));
                getActivity().startActivity(intent);
                break;
            case 1:
                Intent intent1 = new Intent(getActivity(), AddImageToAlbumsActivity.class);
                intent1.putExtra("imageID", getArguments().getString("imageID"));
                getActivity().startActivity(intent1);
                break;
            case 2:
                AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
                alertbox.setMessage("Are you sure you want to delete this image ?");
                alertbox.setTitle("Warning");
                alertbox.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                deleteImageFromServer(getArguments().getString("imageID"));
                            }
                        });
                alertbox.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                //Do nothing
                            }
                        });
                alertbox.show();
        }
    }

    @Override
    public void onAttach(Context context) {
        try{
            Log.e(TAG, "onImageDeletedListener attached");
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastExepction: " + e.getMessage());
        }
        super.onAttach(context);
    }

    public interface ImageDeleteListener {
        void onRefresh();
    }

    public ImageDeleteListener onImageDeleteListener;

    public void setOnImageDeleteListener(ImageDeleteListener onImageDeleteListener){
        this.onImageDeleteListener = onImageDeleteListener;
    }

    public ImageDeleteListener getOnImageDeleteListener() {
        return onImageDeleteListener;
    }

}
