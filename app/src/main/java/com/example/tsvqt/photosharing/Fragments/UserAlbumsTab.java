package com.example.tsvqt.photosharing.Fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Activities.CreateAlbumActivity;
import com.example.tsvqt.photosharing.Adapters.AlbumAdapter;
import com.example.tsvqt.photosharing.Adapters.UserImagesAdapter;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.ObjectHelpers.ProfileImages;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_FIRST_USER;
import static android.app.Activity.RESULT_OK;

public class UserAlbumsTab extends Fragment {

    private static final String TAG = UserAlbumsTab.class.getSimpleName();

    public UserAlbumsTab() {
        // Required empty public constructor
    }

    public static UserAlbumsTab newInstance() {
        UserAlbumsTab fragment = new UserAlbumsTab();
        return fragment;
    }

    private AlbumAdapter adapter;
    private ArrayList<AlbumData> albumList;
    private RecyclerView mRecyclerView;
    private TextView tvNoAlbumsFound;

    private String userID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        albumList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_albums_tab, container, false);

        tvNoAlbumsFound = view.findViewById(R.id.tvNoAlbumsFound);
        tvNoAlbumsFound.setVisibility(View.GONE);

        mRecyclerView = view.findViewById(R.id.rvAlbums);
        adapter = new AlbumAdapter(getActivity(), albumList);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getAlbumListFromServer();

        return view;
    }

    public void onRefresh(){
        Log.e(TAG, "IT WORKS!");
        getAlbumListFromServer();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED){
            return;
        }
        if(requestCode == 1){
            if(resultCode == RESULT_OK) {

            }else if(resultCode == RESULT_FIRST_USER){
                Log.e(TAG, "--------------------------- IT WORKS IN FRAGMETN!");
            }
        }else{
            return;
        }
    }

    public void getAlbumListFromServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_Albums";
        albumList.clear();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ALBUMS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "UserAlbumTab Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    userID = jObj.getString("user_id");
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        JSONArray jArr = jObj.getJSONArray("albums");
                        for(int i = 0; i < jArr.length();i++) {
                            String id = jArr.getJSONObject(i).getString("id");
                            String title = jArr.getJSONObject(i).getString("title");
                            String album_image = jArr.getJSONObject(i).getString("album_image");
                            String created_at = jArr.getJSONObject(i).getString("created_at");
                            String imageCount = jArr.getJSONObject(i).getString("image_count");
                            boolean isIn = jArr.getJSONObject(i).getBoolean("isIn");
                            String description = jArr.getJSONObject(i).getString("description");
                            String tags = jArr.getJSONObject(i).getString("tags");
                            String access = jArr.getJSONObject(i).getString("access");

                            AlbumData image = new AlbumData(id, title, album_image, created_at, imageCount, isIn, userID, description,tags,access);
                            albumList.add(image);
                        }

                    } else {
                        // Error. Get the error message
                        //String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                        //Toast.makeText(getActivity().getApplicationContext(),
                        //       errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                if(albumList.size() == 0){
                    tvNoAlbumsFound.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }else {
                    tvNoAlbumsFound.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    adapter.notifyDataSetChanged();
                }
                adapter.notifyDataSetChanged();
                Log.e(TAG, "adapter is refreshed");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "UserAlbumTab Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                //ID IS LOGGED USER
                if(getActivity().getIntent() != null) {
                    Intent intent = getActivity().getIntent();
                    if (intent.getStringExtra("userId") == null) {
                        params.put("userID", SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID));
                    } else {
                        params.put("userID", intent.getStringExtra("userId"));
                    }
                    params.put("imageID", "NULL");
                }
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
