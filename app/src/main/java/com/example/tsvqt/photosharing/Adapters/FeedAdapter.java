package com.example.tsvqt.photosharing.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.Activities.CommentsActivity;
import com.example.tsvqt.photosharing.Activities.OpenPictureActivity;
import com.example.tsvqt.photosharing.Activities.OpenUserActivity;
import com.example.tsvqt.photosharing.ObjectHelpers.userDataForImageListView;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.ObjectHelpers.ImageDataForImageListView;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder>{

    private static final String TAG = FeedAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<ImageDataForImageListView> imageList = Collections.emptyList();
    private Context context;

    public FeedAdapter(Context context, List<ImageDataForImageListView> imageList){
        inflater = LayoutInflater.from(context);
        this.imageList = imageList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.home_images_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder called " + position);

        ImageDataForImageListView image = imageList.get(position);

        holder.tvUserName.setText(image.getUserData().getFullName());
        holder.tvImageTitle.setText(image.getImageTitle());
        Glide.with(context).load(image.getImgUrl()).into(holder.ivPhoto);
        Glide.with(context).load(image.getUserData().getUserProfilePictureUrl()).apply(RequestOptions.circleCropTransform()).into(holder.ivUserAvatar);
        holder.tvFavCounter.setText(image.getFavCounter());
        holder.tvCommentsCounter.setText(image.getCommentsCounter());
        if(image.isFaved()){
            holder.ivFavoutire.setImageResource(R.drawable.ic_star_on);
        }else{
            holder.ivFavoutire.setImageResource(R.drawable.ic_star_off);
        }

        holder.tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageDataForImageListView img = imageList.get(position);
                Intent intent = new Intent(context, OpenUserActivity.class);
                intent.putExtra("userId", img.getUserID());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.ivUserAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageDataForImageListView img = imageList.get(position);
                Intent intent = new Intent(context, OpenUserActivity.class);
                intent.putExtra("userId", img.getUserID());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageDataForImageListView img = imageList.get(position);
                Intent intent = new Intent(context, OpenPictureActivity.class);
                intent.putExtra("imageID", img.getImgID());
                context.startActivity(intent);
            }
        });

        holder.ivFavoutire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageDataForImageListView img = imageList.get(position);
                if(img.isFaved()){
                    int count = Integer.valueOf(holder.tvFavCounter.getText().toString());
                    count--;
                    holder.tvFavCounter.setText("" + count);
                    holder.ivFavoutire.setImageResource(R.drawable.ic_star_off);
                    img.setFaved(false);
                }else{
                    int count = Integer.valueOf(holder.tvFavCounter.getText().toString());
                    count++;
                    holder.tvFavCounter.setText("" + count);
                    holder.ivFavoutire.setImageResource(R.drawable.ic_star_on);
                    img.setFaved(true);
                }
                updateImgFavCounter(img.getImgID());
                //TODO get loggedIN user images that he liked, think it.
                //TODO change the icons from off to on, and if pressed again remove the fav.
            }
        });

        holder.ivComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageDataForImageListView img = imageList.get(position);
                Intent intent = new Intent(context, CommentsActivity.class);
                intent.putExtra("imageID", img.getImgID());
                context.startActivity(intent);
            }
        });

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Share!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvUserName, tvImageTitle, tvFavCounter, tvCommentsCounter;
        private ImageView ivUserAvatar, ivPhoto, ivFavoutire, ivComments, ivShare;

        public ViewHolder(View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvImageTitle = itemView.findViewById(R.id.tvUploadDate);
            ivPhoto = itemView.findViewById(R.id.ivPhoto);
            ivUserAvatar = itemView.findViewById(R.id.ivUserAvatar);
            tvFavCounter = itemView.findViewById(R.id.tvFavCount);
            tvCommentsCounter = itemView.findViewById(R.id.tvCommentsCount);
            ivFavoutire = itemView.findViewById(R.id.ivFavourite);
            ivComments = itemView.findViewById(R.id.ivComments);
            ivShare = itemView.findViewById(R.id.ivShare);
        }
    }

    private void updateImgFavCounter(final String imageID){
        // Tag used to cancel the request
        String tag_string_req = "req_UpdateFavCounter";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATE_FAVOURIES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, " Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);

                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {

                    } else {
                        // Error. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(context,
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                Map<String, String> user = SharedPreferencesHandler.getUserDetails();
                params.put("imageID", imageID);
                params.put("userID", user.get(SharedPreferencesHandler.KEY_UID));

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
