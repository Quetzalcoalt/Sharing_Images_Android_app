package com.example.tsvqt.photosharing.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tsvqt.photosharing.ObjectHelpers.CustomTextView;
import com.example.tsvqt.photosharing.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SearchTab#} factory method to
 * create an instance of this fragment.
 */
public class SearchTab extends Fragment {

    private static final String TAG = SearchTab.class.getSimpleName();
    private SectionsPagerAdapter mSectionsPagerAdapter;
    //private OnFragmentInteractionListener mListener;

    private TabLayout tbSearch;
    private ViewPager vpSearch;
    private EditText etSearch;
    private ImageView ivDeleteInput;

    public SearchTab() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_tab, container, false);

        tbSearch = view.findViewById(R.id.tbSearch);
        vpSearch = view.findViewById(R.id.vpSearch);
        etSearch = view.findViewById(R.id.etSearchInput);
        ivDeleteInput = view.findViewById(R.id.ivDeleteImput);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        SearchImagesTab searchImagesTab = SearchImagesTab.newInstance();
        SearchImagesTab.clearList();
        SearchUsersTab searchUsersTab = SearchUsersTab.newInstance();
        SearchUsersTab.clearList();
        SearchUserAlbumsTab userAlbumsTab = SearchUserAlbumsTab.newInstance();
        SearchUserAlbumsTab.clearList();

        mSectionsPagerAdapter.addFragment(searchImagesTab, "Images");
        mSectionsPagerAdapter.addFragment(searchUsersTab, "Users");
        mSectionsPagerAdapter.addFragment(userAlbumsTab, "Albums");
        vpSearch.setAdapter(mSectionsPagerAdapter);
        vpSearch.setOffscreenPageLimit(3);
        tbSearch.setVisibility(View.GONE);

        tbSearch.setupWithViewPager(vpSearch);
        tbSearch.setTabGravity(TabLayout.GRAVITY_FILL);

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tbSearch.setVisibility(View.VISIBLE);
                return false;
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //do what you want on the press of 'done'
                    //call each viewpager, so update the details from whats typed
                    String search = etSearch.getText().toString();
                    SearchImagesTab searchImagesTab = (SearchImagesTab)mSectionsPagerAdapter.getItem(0);
                    searchImagesTab.getImageListFromServer(search);
                    SearchUsersTab searchUsersTab = (SearchUsersTab)mSectionsPagerAdapter.getItem(1);
                    searchUsersTab.searchUsersFromServer(search);
                    SearchUserAlbumsTab searchUserAlbumsTab = (SearchUserAlbumsTab)mSectionsPagerAdapter.getItem(2);
                    searchUserAlbumsTab.searchAlbumListFromServer(search);
                    //feedTab.getImageListFromServer(etSearch.getText().toString());
                    //searchUsersTab.getAlbumListFromServer(etSearch.getText().toString());
                    //userAlbumsTab.getAlbumListFromServer(etSearch.getText().toString());
                }
                return false;
            }
        });

        ivDeleteInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
                //SearchImagesTab searchImagesTab = (SearchImagesTab)mSectionsPagerAdapter.getItem(0);
                SearchImagesTab.clearList();
                SearchUsersTab.clearList();
                SearchUserAlbumsTab.clearList();
            }
        });

        return view;
    }
/*
    public void refresh(){
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
        Log.e(TAG, "DOES IT WORK ?");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SearchTab.OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

*/


    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private final SparseArray<WeakReference<Fragment>> instantiatedFragments = new SparseArray<>();
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title){
            if(!mFragmentList.contains(fragment)) {
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            }
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @NonNull
        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            final Fragment fragment = (Fragment) super.instantiateItem(container, position);
            instantiatedFragments.put(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Override
        public void destroyItem(final ViewGroup container, final int position, final Object object) {
            instantiatedFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
