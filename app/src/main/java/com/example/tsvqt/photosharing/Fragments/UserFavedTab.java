package com.example.tsvqt.photosharing.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Adapters.FeedAdapter;
import com.example.tsvqt.photosharing.ObjectHelpers.ImageDataForImageListView;
import com.example.tsvqt.photosharing.ObjectHelpers.userDataForImageListView;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserFavedTab extends Fragment {

    private static final String TAG = UserFavedTab.class.getSimpleName();

    //TODO when user has 0 images show another fragment or something

    public UserFavedTab() {
        // Required empty public constructor
    }

    public static UserFavedTab newInstance() {
        return new UserFavedTab();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUserHomeListView = new ArrayList<>();
    }

    private ArrayList<ImageDataForImageListView> imageUserHomeListView;
    private FeedAdapter adapter;
    private RecyclerView mRecyclerView;
    private TextView tvNoImagesFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_faved_tab, container, false);

        tvNoImagesFound = view.findViewById(R.id.tvNoImages);
        tvNoImagesFound.setVisibility(View.GONE);

        mRecyclerView = view.findViewById(R.id.rvFavedTab);
        adapter = new FeedAdapter(getActivity(), imageUserHomeListView);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setNestedScrollingEnabled(true);

        getImageListFromServer();

        return view;
    }

    public void onRefresh(){
        Log.e(TAG, "IT WORKS!");
        //getUserDataFromServer();
    }

    private void getImageListFromServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_homeScreenImages";
        imageUserHomeListView.clear();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_FAVED_IMAGES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, " Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        // data successfully retrieved from server
                        JSONArray jArr = jObj.getJSONArray("images");
                        for(int i = 0; i < jArr.length();i++) {
                            // Now store the user in SQLite
                            JSONObject currentObject = jArr.getJSONObject(i);
                            String id = currentObject.getString("id");
                            String title = currentObject.getString("title");
                            String user_id = currentObject.getString("user_id");
                            String created_at = currentObject.getString("created_at");
                            String cover_image = currentObject.getString("cover_image");
                            String access = currentObject.getString("access");
                            String favCounter = currentObject.getString("favCounter");
                            String commentsCounter = currentObject.getString("commentsCounter");
                            boolean isFaved = true;

                            JSONObject user = jArr.getJSONObject(i).getJSONObject("user");
                            String uid = user.getString("id");
                            String fName = user.getString("fName");
                            String lName = user.getString("lName");
                            String avatar_image = user.getString("avatar_image");

                            userDataForImageListView userObj = new userDataForImageListView(uid,fName,lName,avatar_image);
                            ImageDataForImageListView image = new ImageDataForImageListView(id,title,user_id,cover_image,created_at,access, favCounter, commentsCounter, isFaved, userObj);
                            imageUserHomeListView.add(image);
                        }
                    } else {
                        // Error. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Log.d(TAG, errorMsg);

                        //Toast.makeText(getActivity().getApplicationContext(),
                               // errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                if(imageUserHomeListView.size() == 0){
                    tvNoImagesFound.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }else {
                    adapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, " Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                if(getActivity().getIntent() != null) {
                    Intent intent = getActivity().getIntent();
                    if (intent.getStringExtra("userId") == null) {
                        params.put("userID", SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID));
                    } else {
                        params.put("userID", intent.getStringExtra("userId"));
                    }
                }

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
