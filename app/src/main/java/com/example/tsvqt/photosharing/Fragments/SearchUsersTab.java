package com.example.tsvqt.photosharing.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Adapters.SearchUserAdapter;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.ObjectHelpers.UserSearchData;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchUsersTab.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchUsersTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchUsersTab extends Fragment {

    private static final String TAG = SearchUsersTab.class.getSimpleName();

    private static SearchUsersTab instance;
    //private OnFragmentInteractionListener mListener;
    private static ArrayList<UserSearchData> userList;
    private static SearchUserAdapter adapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;


    public SearchUsersTab() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SearchUsersTab newInstance() {
        if(instance == null){
            instance = new SearchUsersTab();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_users_tab, container, false);
        userList = new ArrayList<>();
        mRecyclerView = view.findViewById(R.id.rvSearchUsers);
        adapter = new SearchUserAdapter(getActivity(), userList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    /*public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }*/

    public static void clearList(){
        if(adapter!=null) {
            userList.clear();
            adapter.notifyDataSetChanged();
        }
    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
/*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }
    */

    public void searchUsersFromServer(final String search){
        // Tag used to cancel the request
        String tag_string_req = "req_Albums";
        userList.clear();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SEARCHUSERS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "SearchUserTab Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        JSONArray jArr = jObj.getJSONArray("users");
                        for(int i = 0; i < jArr.length();i++) {
                            String id = jArr.getJSONObject(i).getString("id");
                            String firstName = jArr.getJSONObject(i).getString("first_name");
                            String lastName = jArr.getJSONObject(i).getString("last_name");
                            String avatarImage = jArr.getJSONObject(i).getString("avatar_image");
                            String imageCount = jArr.getJSONObject(i).getString("image_count");
                            String imageViewCount = jArr.getJSONObject(i).getString("image_view_count");
                            String userName = firstName + " " + lastName;
                            UserSearchData user = new UserSearchData(avatarImage,userName,id,imageCount,imageViewCount);
                            userList.add(user);
                        }
                    } else {
                        // Error. Get the error message
                        //String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                        //Toast.makeText(getActivity().getApplicationContext(),
                        //       errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "SearchUserTab Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //ID IS LOGGED USER
                params.put("search", search);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
