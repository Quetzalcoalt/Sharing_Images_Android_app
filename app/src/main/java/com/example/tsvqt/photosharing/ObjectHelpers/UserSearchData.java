package com.example.tsvqt.photosharing.ObjectHelpers;

public class UserSearchData {

    private String image, name, id, countUploadedPhotos, totalImageViews;

    public UserSearchData(String image, String name, String id, String countUploadedPhotos, String totalImageViews) {
        this.image = image;
        this.name = name;
        this.id = id;
        this.countUploadedPhotos = countUploadedPhotos;
        this.totalImageViews = totalImageViews;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getCountUploadedPhotos() {
        return countUploadedPhotos;
    }

    public String getTotalImageViews() {
        return totalImageViews;
    }
}
