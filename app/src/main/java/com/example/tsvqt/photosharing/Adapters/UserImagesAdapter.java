package com.example.tsvqt.photosharing.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.tsvqt.photosharing.Activities.HomeActivity;
import com.example.tsvqt.photosharing.Activities.OpenPictureActivity;
import com.example.tsvqt.photosharing.Fragments.AddDeleteFromAlbumDialogFragment;
import com.example.tsvqt.photosharing.Fragments.UserImagesTab;
import com.example.tsvqt.photosharing.ObjectHelpers.ProfileImages;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import java.util.Collections;
import java.util.List;

public class UserImagesAdapter extends RecyclerView.Adapter<UserImagesAdapter.ViewHolder>{

    private static final String TAG = UserImagesAdapter.class.getSimpleName();

    private LayoutInflater inflater;
    private List<ProfileImages> imageList = Collections.emptyList();
    private Context context;
    private AddDeleteFromAlbumDialogFragment.ImageDeleteListener listener;

    public UserImagesAdapter(Context context, List<ProfileImages> imageList, AddDeleteFromAlbumDialogFragment.ImageDeleteListener listener) {
        inflater = LayoutInflater.from(context);
        this.imageList = imageList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.user_images_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        Log.d(TAG, "onCreateHolder created and returned holder");
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder called " + position);

        ProfileImages image = imageList.get(position);
        Glide.with(context).load(image.getImageURL()).into(holder.ivUserImage);

        holder.ivUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileImages img = imageList.get(position);
                Intent intent = new Intent(context, OpenPictureActivity.class);
                intent.putExtra("imageID", img.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.ivUserImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final ProfileImages img = imageList.get(position);
                FragmentManager manager = ((HomeActivity)context).getSupportFragmentManager();
                AddDeleteFromAlbumDialogFragment fragDialog = AddDeleteFromAlbumDialogFragment.newInstance(img.getId());
                fragDialog.setOnImageDeleteListener(listener);
                if (img.getUser_id().equals(SharedPreferencesHandler.getUserDetails().get(SharedPreferencesHandler.KEY_UID))) {
                    fragDialog.show(manager,"fragment_on_long_press_image");
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivUserImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ivUserImage = itemView.findViewById(R.id.ivUserImage);
        }
    }
}
