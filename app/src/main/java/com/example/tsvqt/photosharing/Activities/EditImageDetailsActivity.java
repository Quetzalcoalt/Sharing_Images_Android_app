package com.example.tsvqt.photosharing.Activities;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.tsvqt.photosharing.Fragments.FeedTab;
import com.example.tsvqt.photosharing.Fragments.UserAlbumsTab;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditImageDetailsActivity extends AppCompatActivity {

    //TODO make the delete button

    private static final String TAG = EditImageDetailsActivity.class.getSimpleName();
    private final String PUBLIC = "Public";
    private final String PRIVATE = "Private";
    private final String FRIENDSONLY = "Friends Only";
    private String imageID;

    private Fragment fragment;
    private EditText etTitle,etDescription,etTags;
    private ImageView ivUploadImage, ivBack;
    private Button editButton, deleteButton;
    private Spinner spinnerUpload;
    private int access = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_image_details);

        fragment = getSupportFragmentManager().findFragmentById(R.id.edit_fragment);
        View fragmentView = fragment.getView();
        ivUploadImage = fragmentView.findViewById(R.id.ivUploadedPhoto);
        ivUploadImage.getLayoutParams().height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        ivBack = findViewById(R.id.ivBack);
        etTitle = fragmentView.findViewById(R.id.etTitle);
        etDescription = fragmentView.findViewById(R.id.etDescription);
        etTags = fragmentView.findViewById(R.id.etTags);
        spinnerUpload = fragmentView.findViewById(R.id.sUpload);
        editButton = fragmentView.findViewById(R.id.bUploadImage);
        deleteButton = findViewById(R.id.bDelete);

        String[] accessArray = new String[]{
                PUBLIC,
                FRIENDSONLY,
                PRIVATE
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<>(fragmentView.getContext(), R.layout.spinner_item, accessArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUpload.setAdapter(adapter);

        Intent previousIntent = getIntent();
        Glide.with(getApplicationContext()).load(AppConfig.URL_IMAGE_STORAGE + previousIntent.getStringExtra("imageName")).into(ivUploadImage);

        imageID = previousIntent.getStringExtra("imageID");
        etTitle.setText(previousIntent.getStringExtra("title"));
        etDescription.setText(previousIntent.getStringExtra("description"));
        etTags.setText(previousIntent.getStringExtra("tags"));
        String acs = previousIntent.getStringExtra("access");
        editButton.setText("Edit");
        spinnerUpload.setSelection(Integer.parseInt(acs));

        ivUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Sorry you can't edit that.", Toast.LENGTH_SHORT).show();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editImageToServer();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteImageFromServer();
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spinnerUpload.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                switch (item) {
                    case PUBLIC:
                        access = 0;
                        break;
                    case FRIENDSONLY:
                        access = 2;
                        break;
                    case PRIVATE:
                        access = 1;
                        break;
                    default:
                        access = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void editImageToServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_UploadImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEIMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "EditImageDetailsActivity Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if(!error) {
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                        ivUploadImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_upload));
                        Intent intent = new Intent();
                        intent.putExtra("title", etTitle.getText().toString());
                        intent.putExtra("description", etDescription.getText().toString());
                        intent.putExtra("tags", etTags.getText().toString());
                        setResult(RESULT_OK, intent);
                        finish();
                        //TODO show the user a text that the image was uploaded successfully and reset the uploadTab.
                    }else{
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "EditImageDetailsActivity Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();

                params.put("title", etTitle.getText().toString());
                params.put("description", etDescription.getText().toString());
                params.put("tags", etTags.getText().toString());
                params.put("access", "" + access);
                params.put("imageID", imageID);
                //IMAGE ID

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void deleteImageFromServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_delete_image";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEIMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "EditImageDetailsActivity Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if(!error) {
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent();
                        setResult(RESULT_FIRST_USER, intent);
                        finish();
                        //TODO show the user a text that the image was uploaded successfully and reset the uploadTab.
                    }else{
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "EditImageDetailsActivity Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                params.put("imageID", imageID);
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
