package com.example.tsvqt.photosharing.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Adapters.AlbumAdapter;
import com.example.tsvqt.photosharing.Adapters.UserImagesAdapter;
import com.example.tsvqt.photosharing.Fragments.AddDeleteFromAlbumDialogFragment;
import com.example.tsvqt.photosharing.Fragments.UserAlbumsTab;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.ObjectHelpers.ProfileImages;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OpenAlbumActivity extends AppCompatActivity {

    private static final String TAG = OpenAlbumActivity.class.getSimpleName();

    private UserImagesAdapter adapter;
    private ArrayList<ProfileImages> profileImagesList;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TextView tvNoImagesFound, tvAlbumTitle;
    private ImageView ivBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_album);
        profileImagesList = new ArrayList<>();

        tvNoImagesFound = findViewById(R.id.tvNoImages);
        tvNoImagesFound.setVisibility(View.GONE);
        tvAlbumTitle = findViewById(R.id.tvAlbumTitle);

        Intent intent = getIntent();
        tvAlbumTitle.setText(intent.getStringExtra("albumTitle"));

        ivBack = findViewById(R.id.ivBack);

        mRecyclerView = findViewById(R.id.rvAlbumImages);
        adapter = new UserImagesAdapter(this, profileImagesList, new AddDeleteFromAlbumDialogFragment.ImageDeleteListener() {
            @Override
            public void onRefresh() {
                Log.e(TAG, "OpenAlbumActivity - UserImagesAdapter listener");
            }
        });
        layoutManager = new GridLayoutManager(this,3);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(adapter);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getImageListFromServer();
    }

    private void getImageListFromServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_userProfileImages";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GET_ALBUM_IMAGES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "OpenAlbumActivity Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {

                        //check these in the helper DB tam onova
                        JSONArray jArr = jObj.getJSONArray("images");
                        for(int i = 0; i < jArr.length();i++) {
                            String id = jArr.getJSONObject(i).getString("id");
                            String title = jArr.getJSONObject(i).getString("title");
                            String cover_image = jArr.getJSONObject(i).getString("cover_image");
                            String user_id = jArr.getJSONObject(i).getString("user_id");

                            ProfileImages image = new ProfileImages(id, title, cover_image, user_id);
                            profileImagesList.add(image);
                        }
                    } else {
                        // Error. Get the error message
                        //String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                        //Toast.makeText(getActivity().getApplicationContext(),
                        //       errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                if(profileImagesList.size() == 0){
                    tvNoImagesFound.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }else {
                    adapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "OpenAlbumActivity Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                //ID IS LOGGED USER
                if(getIntent() != null) {
                    Intent intent = getIntent();
                    params.put("albumID", intent.getStringExtra("albumID"));
                }
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
