package com.example.tsvqt.photosharing.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Adapters.CommentsAdapter;
import com.example.tsvqt.photosharing.ObjectHelpers.CommentsData;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;
import com.example.tsvqt.photosharing.helper.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommentsActivity extends AppCompatActivity {

    private static final String TAG = CommentsActivity.class.getSimpleName();

    private ListView commentListView;
    private EditText etWriteComment;
    private Button bPostComment;
    private TextView tvImageTitle;
    private ImageView ivCommentsBack;

    private CommentsAdapter adapter;

    private static String imageID;
    private ArrayList<CommentsData> commentsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        imageID = getIntent().getStringExtra("imageID");
        commentsData = new ArrayList<>();
        adapter = new CommentsAdapter(getApplicationContext(),R.layout.comment_item, commentsData);

        commentListView = findViewById(R.id.lvComments);
        etWriteComment = findViewById(R.id.etComment);
        bPostComment = findViewById(R.id.bPostComment);
        ivCommentsBack = findViewById(R.id.ivCommentsBack);
        tvImageTitle = findViewById(R.id.ivProfileCoverImage);
        tvImageTitle.setText(getIntent().getStringExtra("imageTitle"));

        commentListView.setAdapter(adapter);

        // Check if user is already logged in or not
        if (!SessionManager.isLoggedIn()) {
            etWriteComment.setVisibility(View.GONE);
            bPostComment.setVisibility(View.GONE);
        }

        bPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> user = SharedPreferencesHandler.getUserDetails();
                String comment = etWriteComment.getText().toString();
                String image_id = getIntent().getStringExtra("imageID");
                String user_id = user.get("uid");
                Utils.hideKeyboard(CommentsActivity.this);
                etWriteComment.setText("");
                writeComment(comment,image_id,user_id);
                adapter.notifyDataSetChanged();
            }
        });

        ivCommentsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getComments(imageID);
    }

    private void getComments(final String image_id) {
        // Tag used to cancel the request
        String tag_string_req = "get_comments";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GET_COMMENT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "CommentsActivity Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        commentsData.clear();
                        JSONArray commentsArr = jObj.getJSONArray("comments");
                        for(int i = 0; i < commentsArr.length(); i++){
                            JSONObject obj = commentsArr.getJSONObject(i);
                            String userName = obj.getString("firstName") + " " + obj.getString("lastName");
                            CommentsData comData = new CommentsData(userName, obj.getString("createdAt"),obj.getString("avatarUrl"), obj.getString("comment"), obj.getString("user_id"));
                            commentsData.add(comData);
                        }
                        Log.d(TAG,"CommentsActivity successfully got all comments");
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Log.d(TAG, "CommentsActivity Response: " + errorMsg);
                        Toast.makeText(getApplicationContext(),
                                "Error getting comments", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error getting comments", Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "CommentsActivity Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Error getting comments", Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to getComments for image.
                Map<String, String> params = new HashMap<String, String>();
                if(image_id.equals("")){
                    Log.d(TAG, "CommentsActivity ERROR: image_ID is NULL");
                }
                params.put("imageID", image_id);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void writeComment(final String comment, final String image_id, final String user_id) {
        // Tag used to cancel the request
        String tag_string_req = "writing_comment";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_INSERT_COMMENT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "CommentsActivity Response: " + response.toString());;

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        Log.d(TAG,"CommentsActivity successfully inserted a comment and getting comments to refresh adapter list");
                        //Updating the comments Array and notifying the adapter
                        getComments(imageID);
                    } else {
                        String errorMsg = jObj.getString("error_msg");
                        Log.d(TAG, "CommentsActivity Response: " + errorMsg);
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error posting comment", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "CommentsActivity Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Error posting comment", Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("comment", comment);
                params.put("approved", "1");
                params.put("image_id", image_id);
                params.put("user_id", user_id);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
