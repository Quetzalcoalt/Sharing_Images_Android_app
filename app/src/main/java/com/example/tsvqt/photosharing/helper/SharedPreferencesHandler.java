package com.example.tsvqt.photosharing.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.HashMap;

public class SharedPreferencesHandler {

    private static final String TAG = SharedPreferencesHandler.class.getSimpleName();

    public static final String KEY_FNAME = "fName";
    public static final String KEY_LNAME = "lName";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_UID = "uid";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_AVATAR_IMAGE = "avatar_image";
    public static final String KEY_USER_PAGE_IMAGE = "user_page_image";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public SharedPreferencesHandler() {

    }

    public static void init(Context context) {
        if(sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
    }

    /**
     * This should only be called when user changes on of the following below.
     * */
    public static void updateUser(String fName,String lName, String avatarImage, String userPageImage) {
        editor.putString(KEY_FNAME, fName);
        editor.putString(KEY_LNAME, lName);
        editor.putString(KEY_AVATAR_IMAGE, avatarImage);
        editor.putString(KEY_USER_PAGE_IMAGE, userPageImage);
        editor.commit();

        Log.d(TAG, "User details updated into sharedPreferences");
    }

    /**
     * Adds user data to the SharedPreferences
     * */
    public static void addUser(String fName,String lName, String email, String uid, String created_at, String avatarImage, String userPageImage) {
        editor.putString(KEY_FNAME, fName);
        editor.putString(KEY_LNAME, lName);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_UID, uid);
        editor.putString(KEY_CREATED_AT, created_at);
        editor.putString(KEY_AVATAR_IMAGE, avatarImage);
        editor.putString(KEY_USER_PAGE_IMAGE,userPageImage);
        editor.commit();

        Log.d(TAG, "New user inserted into sharedPreferences: " + uid);
    }

    /**
     * Getting user data from database
     * */
    public static HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_FNAME, sharedPreferences.getString(KEY_FNAME, "null"));
        user.put(KEY_LNAME, sharedPreferences.getString(KEY_LNAME, "null"));
        user.put(KEY_EMAIL, sharedPreferences.getString(KEY_EMAIL, "null"));
        user.put(KEY_UID, sharedPreferences.getString(KEY_UID, "null"));
        user.put(KEY_CREATED_AT, sharedPreferences.getString(KEY_CREATED_AT, "null"));
        user.put(KEY_AVATAR_IMAGE, sharedPreferences.getString(KEY_AVATAR_IMAGE, "null"));
        user.put(KEY_USER_PAGE_IMAGE, sharedPreferences.getString(KEY_USER_PAGE_IMAGE, "null"));

        Log.d(TAG, "Fetching user data from SharePreferences: " + user.toString());
        //TODO CHECK FOR NULL
        return user;
    }

    /**
     * Deletes All data from SharedPreferences
     * */
    public static void clearSharedPreferences() {
        editor.clear();
        editor.commit();

        Log.d(TAG, "Deleted all user info from SharePreferences");
    }
}
