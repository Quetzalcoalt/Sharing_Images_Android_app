package com.example.tsvqt.photosharing.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.tsvqt.photosharing.Activities.RegisterActivity;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

public class UploadTab extends Fragment {

    public UploadTab() {
        // Required empty public constructor
    }

    public static UploadTab newInstance() {
        UploadTab fragment = new UploadTab();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private static final String TAG = UploadTab.class.getSimpleName();

    private AlertDialog.Builder alertDialogBuider;
    private final int GALLERY = 1;
    private final int CAMERA = 2;
    private Bitmap uploadImageBitmap;
    private String imageFilePath;
    private ProgressDialog pDialog;

    private final String PUBLIC = "Public";
    private final String PRIVATE = "Private";
    private final String FRIENDSONLY = "Friends Only";
    private ImageView ivUploadImage;
    private EditText etTitle,etDescription,etTags;
    private Button bUploadImage;
    private Spinner spinnerUpload;
    private int access = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(SessionManager.isLoggedIn()) {
            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_upload_tab, container, false);
            pDialog = new ProgressDialog(getContext());
            pDialog.setCancelable(false);

            ivUploadImage = view.findViewById(R.id.ivUploadedPhoto);
            etTitle = view.findViewById(R.id.etTitle);
            etDescription = view.findViewById(R.id.etDescription);
            etTags = view.findViewById(R.id.etTags);
            bUploadImage = view.findViewById(R.id.bUploadImage);
            spinnerUpload = view.findViewById(R.id.sUpload);

            String[] accessArray = new String[]{
                    PUBLIC,
                    FRIENDSONLY,
                    PRIVATE
            };

            ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(), R.layout.spinner_item, accessArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerUpload.setAdapter(adapter);

            ivUploadImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialogBuider = new AlertDialog.Builder(getContext());
                    alertDialogBuider.setTitle("Choose from")
                            .setItems(R.array.gallery_camera, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case 0:
                                            choosePhotoFromGallery();
                                            break;
                                        case 1:
                                            takePhotoFromCamera();
                                            break;
                                        default:
                                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    alertDialogBuider.show();
                }
            });

            spinnerUpload.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String item = parent.getItemAtPosition(position).toString();
                    switch (item) {
                        case PUBLIC:
                            access = 0;
                            break;
                        case FRIENDSONLY:
                            access = 2;
                            break;
                        case PRIVATE:
                            access = 1;
                            break;
                        default:
                            access = 0;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            bUploadImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO check all data before upload
                    pDialog.setMessage("Uploading image!");
                    showDialog();
                    uploadImageToServer();
                }
            });
            return view;
        }else{
            return inflater.inflate(R.layout.fragment_no_user, container, false);
        }
    }

    @Override
    public void onAttach(Context context) {
        try{
            //onImageUploadListener = (UploadTab.OnImageUploadListener) getTargetFragment();
            Log.e(TAG, "onImageUploadListener attached");
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastExepction: " + e.getMessage());
        }
        super.onAttach(context);
    }

    private void choosePhotoFromGallery(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try{
            photoFile = createImageFile();
        }catch (IOException e){
            e.printStackTrace();
        }

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));//Send fileUri with intent
        startActivityForResult(takePictureIntent, CAMERA);//start activity for result with CAMERA_REQUEST_CODE
    }

    private File createImageFile() throws IOException{
        String timeStamp = DateFormat.getDateInstance().toString();
        String imageFileName ="IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        imageFilePath = image.getAbsolutePath();

        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == getActivity().RESULT_CANCELED){
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    uploadImageBitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
                    Glide.with(this).load(uploadImageBitmap).into(ivUploadImage);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            uploadImageBitmap = BitmapFactory.decodeFile(imageFilePath);
            Glide.with(this).load(uploadImageBitmap).into(ivUploadImage);

            //TODO make so the imageView has height of wrap_content
        }
    }

    private String bitmapToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes,Base64.DEFAULT);
    }

    private void uploadImageToServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_UploadImage";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPLOADIMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "UploadTab Response: " + response);
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if(!error) {
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                        ivUploadImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_upload));
                        etTitle.setText("");
                        etDescription.setText("");
                        etTags.setText("");
                        spinnerUpload.setId(0);
                        //onImageUploadListener.updateList();
                        onDataChangedListener.onRefresh();
                        //TODO show the user a text that the image was uploaded successfully and reset the uploadTab.
                    }else{
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "UploadTab Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                HashMap<String, String> user = SharedPreferencesHandler.getUserDetails();

                params.put("title", etTitle.getText().toString());
                params.put("description", etDescription.getText().toString());
                params.put("tags", etTags.getText().toString());
                params.put("access", "" + access);
                params.put("image", bitmapToString(uploadImageBitmap));
                params.put("userID", user.get("uid"));
                uploadImageBitmap.recycle();

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public interface DataChangedListener {
        void onRefresh();
    }

    public DataChangedListener onDataChangedListener;

    public void setOnDataChangedListener(DataChangedListener onDataChangedListener){
        this.onDataChangedListener = onDataChangedListener;
    }

    public DataChangedListener getOnDataChangedListener() {
        return onDataChangedListener;
    }
}
