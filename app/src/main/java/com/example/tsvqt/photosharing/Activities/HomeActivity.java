package com.example.tsvqt.photosharing.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.tsvqt.photosharing.Fragments.AddDeleteFromAlbumDialogFragment;
import com.example.tsvqt.photosharing.Fragments.FeedTab;
import com.example.tsvqt.photosharing.Fragments.SearchTab;
import com.example.tsvqt.photosharing.Fragments.SearchUsersTab;
import com.example.tsvqt.photosharing.Fragments.UploadTab;
import com.example.tsvqt.photosharing.Fragments.UserAlbumsTab;
import com.example.tsvqt.photosharing.Fragments.UserFavedTab;
import com.example.tsvqt.photosharing.Fragments.UserImagesTab;
import com.example.tsvqt.photosharing.Fragments.UserProfileTab;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity implements
        UploadTab.DataChangedListener{

    private static final String TAG = HomeActivity.class.getSimpleName();

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private boolean isUserLoggedIn;
    public static Context contextOfApplication;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        contextOfApplication = getApplicationContext();

        mViewPager = findViewById(R.id.viewPager);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        UploadTab uploadTab = new UploadTab();

        mSectionsPagerAdapter.addFragment(new FeedTab());
        mSectionsPagerAdapter.addFragment(new SearchTab());
        mSectionsPagerAdapter.addFragment(new UserProfileTab());
        mSectionsPagerAdapter.addFragment(uploadTab);

        mViewPager.setAdapter(mSectionsPagerAdapter);

        uploadTab.setOnDataChangedListener(this);

        tabLayout = findViewById(R.id.tablayout);

       // mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
       // tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_feed);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_search);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_user);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_upload);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        if (SessionManager.isLoggedIn()) {
            HashMap<String, String> user = SharedPreferencesHandler.getUserDetails();
            //ako e logged in trqbva da vzima i friends only images/ za sega vsi4ko koeto ne e private
            isUserLoggedIn = true;
        }else{
            isUserLoggedIn = false;
        }

    }

    @Override
    public void onRefresh() {
        if(mViewPager != null){
            if(mSectionsPagerAdapter != null){
                Fragment fragment = mSectionsPagerAdapter.getItem(2);
                if(fragment !=null){
                    UserProfileTab userProfileTab = (UserProfileTab) fragment;
                    userProfileTab.onRefresh();
                }
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragmentList;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragmentList = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment){
            mFragmentList.add(fragment);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

    }

}
