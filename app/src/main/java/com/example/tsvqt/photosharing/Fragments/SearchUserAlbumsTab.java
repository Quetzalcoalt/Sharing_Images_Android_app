package com.example.tsvqt.photosharing.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tsvqt.photosharing.Adapters.AlbumAdapter;
import com.example.tsvqt.photosharing.Adapters.SearchAlbumAdapter;
import com.example.tsvqt.photosharing.ObjectHelpers.AlbumData;
import com.example.tsvqt.photosharing.ObjectHelpers.SearchAlbumData;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchUserAlbumsTab} interface
 * to handle interaction events.
 * Use the {@link SearchUserAlbumsTab#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchUserAlbumsTab extends Fragment {

    private static final String TAG = SearchUserAlbumsTab.class.getSimpleName();
    private static SearchUserAlbumsTab instance;

    public SearchUserAlbumsTab() {
        // Required empty public constructor
    }

    public static SearchUserAlbumsTab newInstance() {
        if(instance == null) {
            instance = new SearchUserAlbumsTab();
        }
        return instance;
    }

    private static SearchAlbumAdapter adapter;
    private static ArrayList<SearchAlbumData> albumList;
    private RecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        albumList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_user_albums_tab, container, false);

        mRecyclerView = view.findViewById(R.id.rvSearchAlbums);
        adapter = new SearchAlbumAdapter(getActivity(), albumList);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    public static void clearList(){
        if(adapter!=null) {
            albumList.clear();
            adapter.notifyDataSetChanged();
        }
    }

    public void searchAlbumListFromServer(final String search){
        // Tag used to cancel the request
        String tag_string_req = "req_Albums";
        albumList.clear();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SEARCHIALBUMS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "UserAlbumTab Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        JSONArray jArr = jObj.getJSONArray("albums");
                        for(int i = 0; i < jArr.length();i++) {
                            String id = jArr.getJSONObject(i).getString("id");
                            String title = jArr.getJSONObject(i).getString("title");
                            String album_image = jArr.getJSONObject(i).getString("album_image");
                            String created_at = jArr.getJSONObject(i).getString("created_at");
                            String imageCount = jArr.getJSONObject(i).getString("image_count");
                            String description = jArr.getJSONObject(i).getString("description");
                            String tags = jArr.getJSONObject(i).getString("tags");
                            String access = jArr.getJSONObject(i).getString("access");

                            SearchAlbumData image = new SearchAlbumData(id, title, album_image, created_at, imageCount, description,tags,access);
                            albumList.add(image);
                        }
                    } else {
                        // Error. Get the error message
                        //String errorMsg = jArr.getJSONObject(0).getString("error_msg");
                        //Toast.makeText(getActivity().getApplicationContext(),
                        //       errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "UserAlbumTab Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                //ID IS LOGGED USER
                params.put("search", search);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
