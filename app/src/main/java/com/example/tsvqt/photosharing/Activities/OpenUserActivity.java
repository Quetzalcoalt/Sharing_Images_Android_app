package com.example.tsvqt.photosharing.Activities;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OpenUserActivity extends AppCompatActivity {

    private static final String TAG = OpenUserActivity.class.getSimpleName();

    private Fragment fragment;
    private Toolbar toolBarUserProfile;
    private ImageView ivProfileCoverImage, ivUserAvatar, ivBack;
    private TextView tvUserName, tvImageCount;
    private String fName, lName, avatarURL, userPageImageURL, email, imageCount, userID, created_at;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_user);


        ivBack = findViewById(R.id.ivBack);

        fragment = getSupportFragmentManager().findFragmentById(R.id.open_user_fragment);
        View fragmentView = fragment.getView();
        ivProfileCoverImage = fragmentView.findViewById(R.id.ivProfileCoverImage);
        ivUserAvatar = fragmentView.findViewById(R.id.ivUserAvatar);
        tvUserName = fragmentView.findViewById(R.id.tvUserName);
        tvImageCount = fragmentView.findViewById(R.id.tvUploadedImageCount);

        toolBarUserProfile = fragmentView.findViewById(R.id.tool_bar_user_profile);
        toolBarUserProfile.getMenu().clear();
        toolBarUserProfile.inflateMenu(R.menu.menu_user_profile);
        toolBarUserProfile.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_about:
                        Intent intent = new Intent(OpenUserActivity.this, UserAboutActivity.class);
                        intent.putExtra("imgCount", imageCount);
                        intent.putExtra(SharedPreferencesHandler.KEY_UID, userID);
                        intent.putExtra(SharedPreferencesHandler.KEY_CREATED_AT, created_at);
                        startActivity(intent);
                        return true;
                }
                return false;
            }
        });

        CollapsingToolbarLayout collapsingToolbarLayout = fragmentView.findViewById(R.id.col_bar_user_profile);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        getUserDataFromServer();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_user_profile, menu);
        // return true so that the menu pop up is opened
        return true;
    }

    private void getUserDataFromServer(){
        // Tag used to cancel the request
        String tag_string_req = "req_userProfileData";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_USERPROFILE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "OpenUserActivity Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        // data successfully retrieved from server
                        JSONObject user = jObj.getJSONObject("user");
                        userID = user.getString("id");
                        fName = user.getString("fName");
                        lName = user.getString("lName");
                        email = user.getString("email");
                        avatarURL = user.getString("avatar_image");
                        userPageImageURL = user.getString("user_page_image");
                        imageCount = user.getString("imageCount");
                        created_at = user.getString("created_at");
                        //TODO FIX the php code

                        Glide.with(getApplicationContext()).load(AppConfig.URL_AVATAR_STORAGE + avatarURL).apply(RequestOptions.circleCropTransform()).into(ivUserAvatar);
                        Glide.with(getApplicationContext()).load(AppConfig.URL_USER_PAGE_IMAGE + userPageImageURL).apply(RequestOptions.centerCropTransform()).into(ivProfileCoverImage);
                        tvUserName.setText(fName + " " + lName);
                        tvImageCount.setText("Images " + imageCount);
                    } else {
                        //Error. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "OpenUserActivity Error: " + error.getMessage());
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android home page php url
                Map<String, String> params = new HashMap<String, String>();
                Intent intent = getIntent();
                params.put("userID", intent.getStringExtra("userId"));
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
