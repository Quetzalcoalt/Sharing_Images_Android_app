package com.example.tsvqt.photosharing.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.tsvqt.photosharing.R;
import com.example.tsvqt.photosharing.helper.AppConfig;
import com.example.tsvqt.photosharing.helper.AppController;
import com.example.tsvqt.photosharing.helper.SessionManager;
import com.example.tsvqt.photosharing.helper.SharedPreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;

public class OpenPictureActivity extends AppCompatActivity {

    private static final String TAG = OpenPictureActivity.class.getSimpleName();

    private ImageView ivUserAvatar,ivMainPhoto,ivShare,ivComments,ivBackButton, ivMoreInfo, ivEdit;
    private TextView tvUserName,tvTitle,tvCommentsCount;
    private String imageID, imageTitle, userID, userName, tags, imageDescription,createdAT, imageName, access;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_picture);
        getImageFromServer();

        ivUserAvatar = findViewById(R.id.ivOpenPictureAvatar);
        ivMainPhoto = findViewById(R.id.ivOpenPhoto);
        ivShare = findViewById(R.id.ivOpenPictureShare);
        ivComments = findViewById(R.id.ivOpenPictureComments);
        ivBackButton = findViewById(R.id.ivOpenPictureBackButton);
        ivMoreInfo = findViewById(R.id.ivMoreInfo);
        ivEdit = findViewById(R.id.ivEdit);
        ivEdit.setVisibility(View.GONE);

        tvCommentsCount = findViewById(R.id.tvCommentsCount);
        tvUserName = findViewById(R.id.tvOpenPictureUserName);
        tvTitle = findViewById(R.id.tvOpenPicutreTItle);

        ivUserAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OpenPictureActivity.this, OpenUserActivity.class);
                intent.putExtra("userId", userID);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OpenPictureActivity.this, OpenUserActivity.class);
                intent.putExtra("userId", userID);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        ivComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CommentsActivity.class);
                if(!imageID.equals("")) {
                    intent.putExtra("imageID", imageID);
                    intent.putExtra("imageTitle", imageTitle);
                }else{
                    Log.d(TAG, "OpenPictureActivity ERROR: NO IMAGE ID SENT");
                }
                startActivity(intent);
            }
        });

        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO make sharing
            }
        });

        ivMoreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MoreInfoActivity.class);
                if(!imageID.equals("")) {
                    intent.putExtra("title", imageTitle);
                    intent.putExtra("description", imageDescription);
                    intent.putExtra("uploadDate", createdAT);
                    intent.putExtra("userName", userName);
                    intent.putExtra("tags", tags);
                    //TODO Put a list with all album ids that imageID is in, gonna need albumID, albumImage, albumTitle
                }else{
                    Log.d(TAG, "OpenPictureActivity ERROR: NO IMAGE ID SENT");
                }
                startActivity(intent);
            }
        });

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EditImageDetailsActivity.class);
                intent.putExtra("title", imageTitle);
                intent.putExtra("description", imageDescription);
                intent.putExtra("tags", tags);
                intent.putExtra("access", access);
                intent.putExtra("imageName", imageName);
                intent.putExtra("imageID", imageID);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CANCELED){
            return;
        }
        if(requestCode == 1){
            if(resultCode == RESULT_OK) {
                getImageFromServer();
//                imageTitle = data.getStringExtra("title");
//                imageDescription = data.getStringExtra("description");
//                tags = data.getStringExtra("tags");
//
//                tvTitle.setText(imageTitle);
            }else if(resultCode == RESULT_FIRST_USER){
                finish();
            }
        }else{
            return;
        }
    }
    private void getImageFromServer(){

        String tag_string_req = "req_Picture";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_OPEN_PICTURE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "OpenPictureActivity Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);

                    boolean error = jObj.getBoolean("error");
                    if(!error){
                        JSONObject image = jObj.getJSONObject("image");
                        imageID = image.getString("id");
                        imageTitle = image.getString("title");
                        imageName = image.getString("cover_image");
                        //String createdAt = jObj.getString("created_at");
                        String commentsCount = image.getString("commentsCount");
                        imageDescription = image.getString("description");
                        tags = image.getString("tags");
                        createdAT = image.getString("created_at");
                        access = image.getString("access");

                        JSONObject user = image.getJSONObject("user");
                        userID = user.getString("id");
                        String userFName = user.getString("fName");
                        String userLName = user.getString("lName");
                        userName = userFName + " " + userLName;
                        String userAvatar = user.getString("avatar_image");

                        //Successfully got all the DATA from the JSON file, now filling it in the activity.
                        if (SessionManager.isLoggedIn()) {
                            HashMap<String, String> loggedUser = SharedPreferencesHandler.getUserDetails();
                            if(userID.equals(loggedUser.get("uid"))){
                                ivEdit.setVisibility(View.VISIBLE);
                            }else{
                                ivEdit.setVisibility(View.GONE);
                            }
                        }

                        Glide.with(getApplicationContext()).load(AppConfig.URL_AVATAR_STORAGE + userAvatar).apply(RequestOptions.circleCropTransform()).into(ivUserAvatar);
                        Glide.with(getApplicationContext()).load(AppConfig.URL_IMAGE_STORAGE + imageName).into(ivMainPhoto);
                        tvUserName.setText(userName);
                        tvTitle.setText(imageTitle);
                        tvCommentsCount.setText(commentsCount);

                        Log.d(TAG,"OpenPictureActivity: FINISHED FILLING DATA");
                    }else{
                        String errorMsg = jObj.getString("error_msg");
                        Log.d(TAG,"OpenPictureActivity: ERROR: " + errorMsg);
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error at opening picture", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "OpenPictureActivity Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                       error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to android openPicture php url
                Map<String, String> params = new HashMap<String, String>();
                final String imageID = getIntent().getStringExtra("imageID");
                params.put("imageID", imageID);
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
